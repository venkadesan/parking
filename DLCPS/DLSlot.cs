﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Globalization;
using PLCPS;

namespace DLCPS
{
   public class DLSlot
    {
        public DataSet GetSlotDetails(PLSlot objplslot)
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("DBCPS");
                DbCommand cmd = db.GetStoredProcCommand("Qry_GetSlotDetails");
                db.AddInParameter(cmd, "@CompanyId", DbType.Int32, objplslot.companyid);
                db.AddInParameter(cmd, "@LocationId", DbType.Int32, objplslot.locationid);
                db.AddInParameter(cmd, "@SlotId", DbType.Int32, objplslot.slotid);
                db.AddInParameter(cmd, "@Type", DbType.String, objplslot.Type);
                ds = db.ExecuteDataSet(cmd);
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string[] InsertUpdateSlot(PLSlot objplslot)
        {
            try
            {
                string[] Values = new string[3];
                Database db = DatabaseFactory.CreateDatabase("DBCPS");
                DbCommand cmd = db.GetStoredProcCommand("Sp_InsertUpdateSlotDet");
                db.AddInParameter(cmd, "@SlotId", DbType.Int32, objplslot.slotid);
                db.AddInParameter(cmd, "@Slotname", DbType.String, objplslot.slotname);
                db.AddInParameter(cmd, "@CompanyId", DbType.Int32, objplslot.companyid);
                db.AddInParameter(cmd, "@LocationId", DbType.Int32, objplslot.locationid);
                db.AddInParameter(cmd, "@Uid", DbType.Int32, objplslot.UserId);
                db.AddInParameter(cmd, "@Keyno", DbType.String, objplslot.KeyId);

                db.AddInParameter(cmd, "@VariantId", DbType.Int32, objplslot.VariantId);
                db.AddInParameter(cmd, "@Parkingtype", DbType.Int32, objplslot.ParkingType);

                db.AddOutParameter(cmd, "@ErrorMessage", DbType.String, 1000);
                db.AddOutParameter(cmd, "@Status", DbType.String, 10);
                db.ExecuteNonQuery(cmd);
                Values[0] = string.Format(CultureInfo.CurrentCulture, "{0}",
                db.GetParameterValue(cmd, "@ErrorMessage"));
                Values[1] = db.GetParameterValue(cmd, "@Status").ToString();
                return Values;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string DeleteSlotDetails(PLSlot objplslot)
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("DBCPS");
                DbCommand cmd = db.GetStoredProcCommand("Sp_DeleteSlotDet");
                db.AddInParameter(cmd, "@SlotId", DbType.Int32, objplslot.slotid);
                db.AddOutParameter(cmd, "@ErrorMessage", DbType.String, 1000);
                db.ExecuteNonQuery(cmd);
                string results = string.Format(CultureInfo.CurrentCulture, "{0}",
                db.GetParameterValue(cmd, "@ErrorMessage"));
                return results;
            }
            catch (Exception)
            {
                throw;
            }
        }


        public string[] InsertUpdateTransaction(PLSlot objplslot)
        {
            try
            {
                string[] Values = new string[3];
                Database db = DatabaseFactory.CreateDatabase("DBCPS");
                DbCommand cmd = db.GetStoredProcCommand("Sp_InsertUpdateTransaction");
                db.AddInParameter(cmd, "@Type", DbType.String, objplslot.Type);
                db.AddInParameter(cmd, "@RFICardNo", DbType.String, objplslot.RFIDNo);
                db.AddInParameter(cmd, "@SlotId", DbType.Int32, objplslot.slotid);
                db.AddInParameter(cmd, "@KeyNo", DbType.String, objplslot.KeyNo);
                db.AddInParameter(cmd, "@RegNo", DbType.String, objplslot.RegNo);
                db.AddInParameter(cmd, "@DtTime", DbType.DateTime, objplslot.InOutDateTime);

                db.AddInParameter(cmd, "@CompanyId", DbType.Int32, objplslot.companyid);
                db.AddInParameter(cmd, "@LocationId", DbType.Int32, objplslot.locationid);
                db.AddInParameter(cmd, "@Guid", DbType.String, objplslot.GuidNo);
                db.AddInParameter(cmd, "@Uid", DbType.Int32, objplslot.UserId);

                db.AddInParameter(cmd, "@DriverName", DbType.String, objplslot.Drivername);

                db.AddOutParameter(cmd, "@ErrorMessage", DbType.String, 1000);
                db.AddOutParameter(cmd, "@Status", DbType.String, 10);
                db.ExecuteNonQuery(cmd);
                Values[0] = string.Format(CultureInfo.CurrentCulture, "{0}",
                db.GetParameterValue(cmd, "@ErrorMessage"));
                Values[1] = db.GetParameterValue(cmd, "@Status").ToString();
                return Values;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataSet GetTransactionDetails(PLSlot objplslot)
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("DBCPS");
                DbCommand cmd = db.GetStoredProcCommand("Qry_GetTransactionDetails");
                db.AddInParameter(cmd, "@RFICard", DbType.String, objplslot.RFIDNo);
                ds = db.ExecuteDataSet(cmd);
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataSet GetBarcode(PLSlot objplslot)
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("DBCPS");
                DbCommand cmd = db.GetStoredProcCommand("Qry_GetBarcode");
                db.AddInParameter(cmd, "@LocationId", DbType.Int32, objplslot.locationid);
                ds = db.ExecuteDataSet(cmd);
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataSet Getdashboarddetials(PLSlot objplslot)
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("DBCPS");
                DbCommand cmd = db.GetStoredProcCommand("Qry_Getdashboarddetials");
                db.AddInParameter(cmd, "@LocationId", DbType.Int32, objplslot.locationid);
                db.AddInParameter(cmd, "@DtTime", DbType.DateTime, objplslot.InOutDateTime);
                ds = db.ExecuteDataSet(cmd);
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }


        public DataSet GetSlotDetailsByVariant(PLSlot objplslot)
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("DBCPS");
                DbCommand cmd = db.GetStoredProcCommand("Qry_GetMappedSlots");              
                db.AddInParameter(cmd, "@LocationId", DbType.Int32, objplslot.locationid);
                db.AddInParameter(cmd, "@variantId", DbType.Int32, objplslot.VariantId);
                ds = db.ExecuteDataSet(cmd);
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }


        public DataSet GetFreeAndMappedSlot(PLSlot objplslot)
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("DBCPS");
                DbCommand cmd = db.GetStoredProcCommand("Qry_GetMappedSlotsAndFree");
                db.AddInParameter(cmd, "@LocationId", DbType.Int32, objplslot.locationid);
                db.AddInParameter(cmd, "@variantId", DbType.Int32, objplslot.VariantId);
                db.AddInParameter(cmd, "@ParkingType", DbType.Int32, objplslot.ParkingType);
                ds = db.ExecuteDataSet(cmd);
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #region "Map Multiple Slots"

        public string[] MapMultipleSlots(PLSlot objplslot)
        {
            try
            {
                string[] Values = new string[3];
                Database db = DatabaseFactory.CreateDatabase("DBCPS");
                DbCommand cmd = db.GetStoredProcCommand("Sp_MapMultipleSlots");
                db.AddInParameter(cmd, "@locationid", DbType.Int32, objplslot.locationid);
                db.AddInParameter(cmd, "@variantid", DbType.Int32, objplslot.VariantId);
                db.AddInParameter(cmd, "@parkingtypeid", DbType.Int32, objplslot.ParkingType);
                db.AddInParameter(cmd, "@SlotXML", DbType.String, objplslot.SlotXML);

                db.AddOutParameter(cmd, "@ErrorMessage", DbType.String, 1000);
                db.AddOutParameter(cmd, "@Status", DbType.String, 10);
                db.ExecuteNonQuery(cmd);
                Values[0] = string.Format(CultureInfo.CurrentCulture, "{0}",
                db.GetParameterValue(cmd, "@ErrorMessage"));
                Values[1] = db.GetParameterValue(cmd, "@Status").ToString();
                return Values;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion 


        #region "Report Details"

        public DataSet GetReportDet(PLSlot objplslot)
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("DBCPS");
                DbCommand cmd = db.GetStoredProcCommand("Qry_GetReportDet");
                db.AddInParameter(cmd, "@LocationId", DbType.Int32, objplslot.locationid);
                db.AddInParameter(cmd, "@Fromdate", DbType.DateTime, objplslot.FromDate);
                db.AddInParameter(cmd, "@ToDate", DbType.DateTime, objplslot.ToDate);
                ds = db.ExecuteDataSet(cmd);
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion
    }
}
