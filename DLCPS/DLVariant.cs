﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Globalization;
using PLCPS;

namespace DLCPS
{
    public class DLVariant
    {
        public DataSet Getvariants(PLvariant objvariant)
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("DBCPS");
                DbCommand cmd = db.GetStoredProcCommand("Qry_Getvariants");
                db.AddInParameter(cmd, "@LocationId", DbType.Int32, objvariant.locationid);
                db.AddInParameter(cmd, "@VariantId", DbType.Int32, objvariant.variantid);
                ds = db.ExecuteDataSet(cmd);
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string[] InsertUpdatevariant(PLvariant objvariant)
        {
            try
            {
                string[] Values = new string[3];
                Database db = DatabaseFactory.CreateDatabase("DBCPS");
                DbCommand cmd = db.GetStoredProcCommand("SP_InsertUpdatevariant");
                db.AddInParameter(cmd, "@variantId", DbType.Int32, objvariant.variantid);
                db.AddInParameter(cmd, "@Varianttype", DbType.String, objvariant.varianttye);
                db.AddInParameter(cmd, "@CompanyId", DbType.Int32, objvariant.companyid);
                db.AddInParameter(cmd, "@LocationId", DbType.Int32, objvariant.locationid);
                db.AddInParameter(cmd, "@Uid", DbType.Int32, objvariant.userid);
                db.AddOutParameter(cmd, "@ErrorMessage", DbType.String, 1000);
                db.AddOutParameter(cmd, "@Status", DbType.String, 10);
                db.ExecuteNonQuery(cmd);
                Values[0] = string.Format(CultureInfo.CurrentCulture, "{0}",
                db.GetParameterValue(cmd, "@ErrorMessage"));
                Values[1] = db.GetParameterValue(cmd, "@Status").ToString();
                return Values;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string Deletevariant(PLvariant objvariant)
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("DBCPS");
                DbCommand cmd = db.GetStoredProcCommand("SP_Deletevariant");
                db.AddInParameter(cmd, "@variantId", DbType.Int32, objvariant.variantid);
                db.AddOutParameter(cmd, "@ErrorMessage", DbType.String, 1000);
                db.ExecuteNonQuery(cmd);
                string results = string.Format(CultureInfo.CurrentCulture, "{0}",
                db.GetParameterValue(cmd, "@ErrorMessage"));
                return results;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
