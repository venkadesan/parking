﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Globalization;
using PLCPS;

namespace DLCPS
{
   public class DLDriver
   {
       public DataSet Getdrivers(PLDriver objdriver)
       {
           try
           {
               DataSet ds = new DataSet();
               Database db = DatabaseFactory.CreateDatabase("DBCPS");
               DbCommand cmd = db.GetStoredProcCommand("Qry_GetDriver");
               db.AddInParameter(cmd, "@LocationId", DbType.Int32, objdriver.locationid);
               db.AddInParameter(cmd, "@driverId", DbType.Int32, objdriver.driverid);
               ds = db.ExecuteDataSet(cmd);
               return ds;
           }
           catch (Exception)
           {
               throw;
           }
       }

       public string[] InsertUpdatedriver(PLDriver objdriver)
       {
           try
           {
               string[] Values = new string[3];
               Database db = DatabaseFactory.CreateDatabase("DBCPS");
               DbCommand cmd = db.GetStoredProcCommand("SP_InsertUpdateDriver");
               db.AddInParameter(cmd, "@driverId", DbType.Int32, objdriver.driverid);
               db.AddInParameter(cmd, "@drivername", DbType.String, objdriver.drivername);
               db.AddInParameter(cmd, "@CompanyId", DbType.Int32, objdriver.companyid);
               db.AddInParameter(cmd, "@LocationId", DbType.Int32, objdriver.locationid);
               db.AddInParameter(cmd, "@Uid", DbType.Int32, objdriver.userid);
               db.AddOutParameter(cmd, "@ErrorMessage", DbType.String, 1000);
               db.AddOutParameter(cmd, "@Status", DbType.String, 10);
               db.ExecuteNonQuery(cmd);
               Values[0] = string.Format(CultureInfo.CurrentCulture, "{0}",
               db.GetParameterValue(cmd, "@ErrorMessage"));
               Values[1] = db.GetParameterValue(cmd, "@Status").ToString();
               return Values;
           }
           catch (Exception)
           {
               throw;
           }
       }

       public string Deletedriver(PLDriver objdriver)
       {
           try
           {
               DataSet ds = new DataSet();
               Database db = DatabaseFactory.CreateDatabase("DBCPS");
               DbCommand cmd = db.GetStoredProcCommand("Sp_DeleteDriverDet");
               db.AddInParameter(cmd, "@driverId", DbType.Int32, objdriver.driverid);
               db.AddOutParameter(cmd, "@ErrorMessage", DbType.String, 1000);
               db.ExecuteNonQuery(cmd);
               string results = string.Format(CultureInfo.CurrentCulture, "{0}",
               db.GetParameterValue(cmd, "@ErrorMessage"));
               return results;
           }
           catch (Exception)
           {
               throw;
           }
       }
    }
}
