﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DLCPS;
using PLCPS;
using System.Data;

namespace BLCPS
{
    public class BLCPS
    {
        DLSlot _objDlSlot = new DLSlot();

        public DataSet GetSlotDetails(PLSlot objSlot)
        {
            try
            {
                return _objDlSlot.GetSlotDetails(objSlot);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string[] InsertUpdateSlot(PLSlot objSlot)
        {
            try
            {
                return _objDlSlot.InsertUpdateSlot(objSlot);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string DeleteSlotDetails(PLSlot objSlot)
        {
            try
            {
                return _objDlSlot.DeleteSlotDetails(objSlot);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
