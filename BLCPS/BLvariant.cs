﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DLCPS;
using PLCPS;
using System.Data;
namespace BLCPS
{
    public class BLvariant
    {
        DLVariant _objDlVariant = new DLVariant();

        public DataSet Getvariants(PLvariant objvariant)
        {
            try
            {
                return _objDlVariant.Getvariants(objvariant);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string[] InsertUpdatevariant(PLvariant objvariant)
        {
            try
            {
                return _objDlVariant.InsertUpdatevariant(objvariant);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string Deletevariant(PLvariant objvariant)
        {
            try
            {
                return _objDlVariant.Deletevariant(objvariant);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
