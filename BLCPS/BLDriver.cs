﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DLCPS;
using PLCPS;
using System.Data;

namespace BLCPS
{
    public class BLDriver
    {
        DLDriver _objDldriver = new DLDriver();

        public DataSet Getdrivers(PLDriver objdriver)
        {
            try
            {
                return _objDldriver.Getdrivers(objdriver);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string[] InsertUpdatedriver(PLDriver objdriver)
        {
            try
            {
                return _objDldriver.InsertUpdatedriver(objdriver);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string Deletedriver(PLDriver objdriver)
        {
            try
            {
                return _objDldriver.Deletedriver(objdriver);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
