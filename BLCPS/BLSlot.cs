﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DLCPS;
using PLCPS;
using System.Data;

namespace BLCPS
{
    public class BLSlot
    {
        DLSlot _objDlSlot = new DLSlot();

        public DataSet GetSlotDetails(PLSlot objSlot)
        {
            try
            {
                return _objDlSlot.GetSlotDetails(objSlot);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string[] InsertUpdateSlot(PLSlot objSlot)
        {
            try
            {
                return _objDlSlot.InsertUpdateSlot(objSlot);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string DeleteSlotDetails(PLSlot objSlot)
        {
            try
            {
                return _objDlSlot.DeleteSlotDetails(objSlot);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataSet GetBarcode(PLSlot objplslot)
        {
            try
            {
                return _objDlSlot.GetBarcode(objplslot);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #region "Transaction"

        public DataSet GetTransactionDetails(PLSlot objSlot)
        {
            try
            {
                return _objDlSlot.GetTransactionDetails(objSlot);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string[] InsertUpdateTransaction(PLSlot objSlot)
        {
            try
            {
                return _objDlSlot.InsertUpdateTransaction(objSlot);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataSet Getdashboarddetials(PLSlot objSlot)
        {
            try
            {
                return _objDlSlot.Getdashboarddetials(objSlot);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataSet GetSlotDetailsByVariant(PLSlot objSlot)
        {
            try
            {
                return _objDlSlot.GetSlotDetailsByVariant(objSlot);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataSet GetFreeAndMappedSlot(PLSlot objSlot)
        {
            try
            {
                return _objDlSlot.GetFreeAndMappedSlot(objSlot);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region "Map Multiple Slots"


        public string[] MapMultipleSlots(PLSlot objSlot)
        {
            try
            {
                return _objDlSlot.MapMultipleSlots(objSlot);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region "Get Report Details"

        public DataSet GetReportDet(PLSlot objSlot)
        {
            try
            {
                return _objDlSlot.GetReportDet(objSlot);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion
    }
}
