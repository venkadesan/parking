﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PLCPS
{
   public class PLDriver
   {
       public int driverid { get; set; }
       public string drivername { get; set; }
       public int companyid { get; set; }
       public int locationid { get; set; }
       public int userid { get; set; }
    }
}
