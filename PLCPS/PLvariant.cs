﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PLCPS
{
    public class PLvariant
    {
        public int variantid { get; set; }
        public string varianttye { get; set; }
        public int companyid { get; set; }
        public int locationid { get; set; }
        public int userid { get; set; }
    }
}
