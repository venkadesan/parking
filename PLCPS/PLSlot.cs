﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PLCPS
{
    public class PLSlot
    {
        public int slotid { get; set; }
        public string slotname { get; set; }
        public string slotshortname { get; set; }
        public int companyid { get; set; }
        public int locationid { get; set; }
        public int UserId { get; set; }
        public string Type { get; set; }

        public string RFIDNo { get; set; }
        public string KeyNo { get; set; }
        public string RegNo { get; set; }
        public DateTime InOutDateTime { get; set; }
        public string GuidNo { get; set; }
        public string KeyId { get; set; }

        public int VariantId { get; set; }
        public int ParkingType { get; set; }

        public string Drivername { get; set; }

        public string SlotXML { get; set; }

        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }
}
