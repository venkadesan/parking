﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTextSharp.text;
using iTextSharp.text.pdf;
using QRCoder;
using System.Drawing;
using System.IO;

namespace CarParkingSystem
{
    public partial class _QRCodeGenerator : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GnerateBarcode();
            }
        }

        private void GnerateBarcode()
        {

            //iTextSharp.text.Image pic = iTextSharp.text.Image.GetInstance(

            iTextSharp.text.Document doc = new Document(new iTextSharp.text.Rectangle(50, 12), 1, 1, 1, 1);
            //PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "/codes.pdf", FileMode.Create));
            iTextSharp.text.pdf.PdfWriter writer = PdfWriter.GetInstance(doc, Response.OutputStream);
            doc.Open();
            System.Drawing.Image img1 = null;
            for (int i = 1; i <= 1; i++)
            {
                if (i != 0)
                    doc.NewPage();
                iTextSharp.text.pdf.PdfContentByte cb1 = writer.DirectContent;
                iTextSharp.text.pdf.PdfContentByte cb11 = writer.DirectContent;
                iTextSharp.text.pdf.BaseFont bf = BaseFont.CreateFont(BaseFont.TIMES_BOLDITALIC, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                PdfContentByte cb2 = writer.DirectContent;
                PdfContentByte cb22 = writer.DirectContent;
                PdfContentByte cb3 = writer.DirectContent;
                PdfContentByte cb33 = writer.DirectContent;
                BaseFont bf1 = BaseFont.CreateFont(BaseFont.TIMES_BOLDITALIC, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

                iTextSharp.text.pdf.PdfContentByte cb = writer.DirectContent;
                iTextSharp.text.pdf.Barcode128 bc = new Barcode128();
                bc.TextAlignment = Element.ALIGN_LEFT;
                bc.Code = (i + 1).ToString();
                bc.StartStopText = false;
                bc.CodeType = iTextSharp.text.pdf.Barcode128.EAN13;
                bc.Extended = true;




                string code = "6987051787094807750";
                QRCodeGenerator qrGenerator = new QRCodeGenerator();
                QRCodeGenerator.QRCode qrCode = qrGenerator.CreateQrCode(code, QRCodeGenerator.ECCLevel.Q);
                System.Web.UI.WebControls.Image imgBarCode = new System.Web.UI.WebControls.Image();
                imgBarCode.Height = 150;
                imgBarCode.Width = 150;
                using (Bitmap bitMap = qrCode.GetGraphic(20))
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        bitMap.Save("E:\\venkadesan\\qrcode.png");
                        //byte[] byteImage = ms.ToArray();
                        //imgBarCode.ImageUrl = "data:image/png;base64," + Convert.ToBase64String(byteImage);
                        //iTextSharp.text.Image qrimg = iTextSharp.text.Image.GetInstance(ms.GetBuffer());
                       
                        //cb.AddImage(qrimg);
                    }
                    plBarCode.Controls.Add(imgBarCode);

                }


                iTextSharp.text.Image img = bc.CreateImageWithBarcode(cb, iTextSharp.text.BaseColor.BLACK, iTextSharp.text.BaseColor.BLACK);
                cb.SetTextMatrix(1.2f, 1.7f);
                img.ScaleToFit(60, 4);
                img.SetAbsolutePosition(1.2f, 1.2f);
                cb.AddImage(img);
            }

            doc.Close();
            //  System.Diagnostics.Process.Start(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "/codes.pdf");
            //MessageBox.Show("Bar codes generated on desktop fileName=codes.pdf");
            //Response.Buffer = true;
            //Response.ContentType = "application/pdf";
            //Response.AddHeader("content-disposition", "attachment;filename=Print Label.pdf");
            //Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //Response.Write(doc);
            //Response.End();
        }
    }
}