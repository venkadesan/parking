﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true"
    CodeBehind="dashboard.aspx.cs" Inherits="CarParkingSystem.dashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content" runat="server">
    <link href="flipcss/css/style.css" rel="stylesheet">
    <%--  <script>
        $(function () {

            // Initialize card flip
            $('.card.hover').hover(function () {
                $(this).addClass('flip');
            }, function () {
                $(this).removeClass('flip');
            });

            //social feed hover function
            $(".social-feed li").on({
                mouseenter: function () {
                    $('.social-feed .active').removeClass('active');
                }, mouseleave: function () {
                    $(this).addClass('active');
                }
            });
        })
      
    </script>--%>
    <script>

        $(document).ready(function () {
            //            $("#divavailable").hide();
            //            $("#divalloted").hide();           
        });
        function HideDetails(type, clientid) {
            var count = document.getElementById(clientid).innerHTML;
            if (count != '0') {
                if (type == 'alloted') {
                    $("#divavailable").css("display", "none");
                    $("#divalloted").css("display", "block");
                }
                else {
                    $("#divalloted").css("display", "none");
                    $("#divavailable").css("display", "block");
                }
            }
        }
    </script>
    <ul id="nav-info" class="clearfix">
        <li>
            <asp:Literal ID="lmainheader" runat="server" Text="Dashboard"></asp:Literal></li>
        <li>
            <asp:Literal ID="lsubheader" runat="server" Text="Dashboard"></asp:Literal></li>
    </ul>
    <div class="alert alert-danger" id="divmsg" visible="false" runat="server">
        <button type="button" class="close" data-dismiss="alert">
            ×</button>
        <asp:Label runat="server" ID="lblError"></asp:Label>
    </div>
    <div class="main">
        <!-- cards -->
        <div class="row cards">
            <div class="card-container col-lg-4 col-md-6 col-sm-12">
                <div class="card card-red hover">
                    <div class="front">
                        <h1>
                            Total Slots</h1>
                        <p id="users-count">
                            <asp:Label ID="ltotalslots" runat="server"></asp:Label></p>
                        <span class="fa-stack fa-2x pull-right"><i class="fa fa-circle fa-stack-2x"></i><i
                            class="fa fa-cogs fa-stack-1x"></i></span>
                    </div>
                    <%--<div class="back">
                        <ul class="inline divided">
                            <li>
                                <h1>
                                    Total Users</h1>
                                <p>
                                    3541</p>
                            </li>
                            <li>
                                <h1>
                                    Last Month</h1>
                                <p>
                                    698</p>
                            </li>
                        </ul>
                        <div class="summary negative">
                            13% <i class="fa fa-arrow-down"></i>this month</div>
                        <!-- Button trigger modal -->
                    </div>--%>
                </div>
            </div>
            <div class="card-container col-lg-4 col-md-6 col-sm-12">
                <div class="card card-cyan hover">
                    <div class="front" onclick="HideDetails('alloted','ltotalalloted')">
                        <h1>
                            Total Alloted Slots</h1>
                        <p id="orders-count">
                            <asp:Label ID="ltotalalloted" runat="server" ClientIDMode="Static"></asp:Label></p>
                        <span class="fa-stack fa-2x pull-right"><i class="fa fa-circle fa-stack-2x"></i><i
                            class="fa fa-car fa-stack-1x"></i></span>
                    </div>
                    <%--<div class="back">
                        <ul class="inline divided">
                            <li>
                                <h1>
                                    Total Orders</h1>
                                <p>
                                    842</p>
                            </li>
                            <li>
                                <h1>
                                    Last Month</h1>
                                <p>
                                    151</p>
                            </li>
                        </ul>
                        <div class="summary positive">
                            6% <i class="fa fa-arrow-up"></i>this month</div>
                    </div>--%>
                </div>
            </div>
            <div class="card-container col-lg-4 col-md-6 col-sm-12">
                <div class="card card-green hover">
                    <div class="front" onclick="HideDetails('available','ltotalavailable')">
                        <h1>
                            Total Available Slots</h1>
                        <p id="sales-count">
                            <asp:Label ID="ltotalavailable" runat="server" ClientIDMode="Static"></asp:Label></p>
                        <span class="fa-stack fa-2x pull-right"><i class="fa fa-circle fa-stack-2x"></i><i
                            class="fa fa-check-circle fa-stack-1x"></i></span>
                    </div>
                    <%--<div class="back">
                        <ul class="inline divided">
                            <li>
                                <h1>
                                    Total Sales</h1>
                                <p>
                                    25,165</p>
                            </li>
                            <li>
                                <h1>
                                    Last Month</h1>
                                <p>
                                    3,154</p>
                            </li>
                        </ul>
                        <div class="summary positive">
                            18% <i class="fa fa-arrow-up"></i>this month</div>
                    </div>--%>
                </div>
            </div>
        </div>
        <div class="form-horizontal form-box-content">
            <div class="row" id="divalloted" style="display: none;">
                <div class="form-group">
                    <div class="col-md-1" style="float: right">
                        <asp:LinkButton ID="lnkalloted" runat="server" ToolTip="To Download XL Document"
                            OnClick="lnkalloted_Click"><i class="gemicon-medium-download"></i></asp:LinkButton>
                    </div>
                </div>
                <asp:Repeater ID="rptralloted" runat="server">
                    <HeaderTemplate>
                        <table id="example-datatables" class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr class="submenu new-color">
                                    <th>
                                        S.No
                                    </th>
                                    <th>
                                        In-Date
                                    </th>
                                    <th>
                                        In-Time
                                    </th>
                                    <th>
                                        RFID CardNo
                                    </th>
                                    <th>
                                        Slot
                                    </th>
                                    <th>
                                        Reg.No
                                    </th>
                                </tr>
                            </thead>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <asp:Label ID="Label2" runat="server" Text='<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>' />
                            </td>
                            <td>
                                <asp:Label ID="InDate" runat="server" Text='<%# Eval("InDate") %>' ToolTip='<%# Eval("InDate") %>' />
                            </td>
                            <td>
                                <asp:Label ID="InTime" runat="server" Text='<%# Eval("InTime") %>' ToolTip='<%# Eval("InTime") %>' />
                            </td>
                            <td>
                                <asp:Label ID="RFICardNo" runat="server" Text='<%# Eval("RFICardNo") %>' ToolTip='<%# Eval("RFICardNo") %>' />
                            </td>
                            <td>
                                <asp:Label ID="slotname" runat="server" Text='<%# Eval("slotname") %>' ToolTip='<%# Eval("slotname") %>' />
                            </td>
                            <td>
                                <asp:Label ID="RegNo" runat="server" Text='<%# Eval("RegNo") %>' ToolTip='<%# Eval("RegNo") %>' />
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
            <div class="row" id="divavailable" style="display: none;">
                <div class="form-group">
                    <div class="col-md-1" style="float: right">
                        <asp:LinkButton ID="lnkavailable" runat="server" ToolTip="To Download XL Document"
                            OnClick="lnkavailable_Click">
                        <i class="gemicon-medium-download"></i></asp:LinkButton>
                    </div>
                </div>
                <asp:Repeater ID="rptravilable" runat="server">
                    <HeaderTemplate>
                        <table id="Table1" class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr class="submenu new-color">
                                    <th>
                                        S.No
                                    </th>
                                    <th>
                                        Slot
                                    </th>
                                    <th>
                                        Key
                                    </th>
                                    <th>
                                        Floor
                                    </th>
                                </tr>
                            </thead>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <asp:Label ID="Label2" runat="server" Text='<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>' />
                            </td>
                            <td>
                                <asp:Label ID="slotname" runat="server" Text='<%# Eval("slotname") %>' ToolTip='<%# Eval("slotname") %>' />
                            </td>
                            <td>
                                <asp:Label ID="keyno" runat="server" Text='<%# Eval("keyno") %>' ToolTip='<%# Eval("keyno") %>' />
                            </td>
                            <td>
                                <asp:Label ID="FloorName" runat="server" Text='<%# Eval("FloorName") %>' ToolTip='<%# Eval("FloorName") %>' />
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>
</asp:Content>
