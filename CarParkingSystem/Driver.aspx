﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Driver.aspx.cs" Inherits="CarParkingSystem.Driver" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content" runat="server">
    <script>
        $(document).ready(function () {
            /* Initialize Datatables */
            $('#example-datatables').dataTable({
                "iDisplayLength": 100
            });
            $('.dataTables_filter input').attr('placeholder', 'Search');

            $('#lidriver').addClass("active");
            //$("#lidepartment").parent().parent().removeClass("dropdown a1");
            $("#lidriver").parent().parent().parent().addClass("active");
        });
    </script>
    <script type="text/javascript">

        $(document).ready(function () {
            $('#lidriver').addClass("active");
            //$("#lidepartment").parent().parent().removeClass("dropdown a1");
            $("#lidriver").parent().parent().addClass("dropdown a1 open");
        });

        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
        
    </script>
    <ul id="nav-info" class="clearfix">
        <li>
            <asp:Literal ID="lmainheader" runat="server" Text="Master"></asp:Literal></li>
        <li>
            <asp:Literal ID="lsubheader" runat="server" Text="Car-Variant"></asp:Literal></li>
        <li>
            <asp:LinkButton ID="bNew" runat="server" ToolTip="Click button to add record" class="hi hi-plus"
                OnClick="bNew_Click" />
        </li>
    </ul>
    <!-- /submenu -->
    <!-- content main container -->
    <div class="alert alert-danger" id="divmsg" visible="false" runat="server">
        <button type="button" class="close" data-dismiss="alert">
            ×</button>
        <asp:Label runat="server" ID="lblError"></asp:Label>
    </div>
    <div class="form-horizontal form-box">
        <h4 class="form-box-header">
            Driver</h4>
        <div class="form-box-content">
            <asp:Panel runat="server" ID="pAdd">
                <div class="form-group">
                    <label class="control-label col-md-2">
                        <asp:Label Text="Driver Name" runat="server" ID="lStatusName" />:</label>
                    <div class="col-md-3">
                        <div class="input-group">
                            <asp:TextBox ID="tdriver" runat="server" TabIndex="1" ToolTip="Please enter the driver"
                                class="form-control"></asp:TextBox>
                        </div>
                    </div>                  
                </div>
                <div class="form-group form-actions">
                    <div class="col-md-10 col-md-offset-2">
                        <asp:Button ID="bSave" Text="Save" runat="server" ToolTip="Click button to save record"
                            TabIndex="3" class="btn btn-success" OnClick="bSave_Click" />
                        <asp:Button ID="bCancel" Text="Cancel" runat="server" ToolTip="Click button to cancel record"
                            TabIndex="4" class="btn btn-danger" OnClick="bCancel_Click" />
                        <asp:HiddenField ID="hdndriverid" Value="0" runat="server" />
                    </div>
                </div>
            </asp:Panel>
            <div class="row">
                <table id="example-datatables" class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr class="submenu new-color">
                            <th>
                                S.No
                            </th>
                            <th>
                                Driver
                            </th>
                            <th style="width: 10%; text-align: center;">
                                Action
                            </th>
                        </tr>
                    </thead>
                    <asp:Repeater ID="rptrdriver" runat="server" OnItemCommand="rptrdriver_ItemCommand">
                        <HeaderTemplate>
                            <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <asp:Label ID="Label2" runat="server" Text='<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>' />
                                </td>
                                <td>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("drivername") %>' ToolTip='<%# Eval("drivername") %>' />
                                </td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <asp:LinkButton ID="imgEdit" class="btn btn-xs btn-success" CommandArgument='<%# Eval("driverid") %>'
                                            ToolTip="Edit" CommandName="edit" runat="server"> <i class="fa fa-pencil"></i></asp:LinkButton>
                                        <asp:LinkButton ID="imgDelete" class="btn btn-xs btn-danger" CommandArgument='<%# Eval("driverid") %>'
                                            ToolTip="Delete" OnClientClick="return confirm('Are you sure to delete?');" CommandName="delete"
                                            runat="server"> <i class="fa fa-times"></i></asp:LinkButton>
                                    </div>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody> </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
