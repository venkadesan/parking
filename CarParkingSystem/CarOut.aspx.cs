﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Web.Services;
using PLCPS;
using BLCPS;
using System.Data;
using CarParkingSystem.Code;


namespace CarParkingSystem
{
    public partial class CarOut : PageBase
    {
        PLSlot _objPlslot = new PLSlot();
        BLSlot _objBlslot = new BLSlot();

        PLDriver _objPldriver = new PLDriver();
        BLDriver _objBldriver = new BLDriver();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindDriver();
                Setheadertextdetails();
            }
        }

        private void BindDriver()
        {
            try
            {
                _objPldriver.driverid = 0;
                DataSet dsdriver = new DataSet();
                _objPldriver.companyid = UserSession.CompanyIDCurrent;
                _objPldriver.locationid = UserSession.LocationIDCurrent;
                _objPldriver.driverid = 0;
                dsdriver = _objBldriver.Getdrivers(_objPldriver);
                if (dsdriver != null && dsdriver.Tables.Count > 0 && dsdriver.Tables[0].Rows.Count > 0)
                {
                    this.ddldriver.DataSource = dsdriver.Tables[0];
                    this.ddldriver.DataTextField = "drivername";
                    this.ddldriver.DataValueField = "driverid";
                    this.ddldriver.DataBind();
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, Common.ErrorType.Error);
            }
        }

        /// <summary>
        /// Sets the Header and Sub Header based on the Link.
        /// </summary>
        private void Setheadertextdetails()
        {
            try
            {
                string mainmenu = string.Empty;
                string submenu = string.Empty;
                string[] file = Request.CurrentExecutionFilePath.Split('/');
                string fileName = file[file.Length - 1];
                Getmainandsubform(fileName, out mainmenu, out submenu);
                if (mainmenu.Trim() == string.Empty)
                    mainmenu = "Transaction";
                if (submenu.Trim() == string.Empty)
                    submenu = "Car-Out";
                lmainheader.Text = mainmenu;
                lsubheader.Text = submenu;
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message.ToString(), Common.ErrorType.Error);
            }
        }


        public class Transaction
        {         
            public string slotname { get; set; }
            public string KeyNo { get; set; }
            public string RegNo { get; set; }
            public string Guid { get; set; }
            public int SlotId { get; set; }
            public string InDrivername { get; set; }
        }

         [WebMethod]
        public static Transaction[] GetTransactionDetails(string rfidno)
        {
            try
            {
                List<Transaction> lstTransaction = new List<Transaction>();
                DataSet dsTransaction = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("DBCPS");
                DbCommand cmd = db.GetStoredProcCommand("Qry_GetTransactionDetails");
                db.AddInParameter(cmd, "@RFICard", DbType.String, rfidno);
                db.AddInParameter(cmd, "@locationId", DbType.String, UserSession.LocationIDCurrent);
                dsTransaction = db.ExecuteDataSet(cmd);
                foreach (DataRow drEmp in dsTransaction.Tables[0].Rows)
                {
                    Transaction objTransaction = new Transaction();
                    objTransaction.slotname = drEmp["slotname"].ToString();
                    objTransaction.KeyNo = drEmp["KeyNo"].ToString();
                    objTransaction.RegNo = drEmp["RegNo"].ToString();
                    objTransaction.Guid = drEmp["Guid"].ToString();
                    objTransaction.SlotId = Convert.ToInt32(drEmp["SlotId"].ToString());
                    objTransaction.InDrivername = drEmp["InDrivername"].ToString();
                    lstTransaction.Add(objTransaction);
                }
                return lstTransaction.ToArray();
            }
            catch (Exception)
            {
                throw;
            }
        }

         protected void bSave_Click(object sender, EventArgs e)
         {
             try
             {
                 int slotid = 0;

                 if (trfidno.Text == string.Empty)
                 {
                     NotifyMessages("Enter RFID No", Common.ErrorType.Error);
                     return;
                 }
                 slotid = Convert.ToInt32(hdnslotid.Value);

                 _objPlslot.Type = "out";
                 _objPlslot.RFIDNo = trfidno.Text.Trim();
                 _objPlslot.slotid = slotid;
                 _objPlslot.companyid = UserSession.CompanyIDCurrent;
                 _objPlslot.locationid = UserSession.LocationIDCurrent;
                 _objPlslot.UserId = UserSession.UserID;
                 _objPlslot.RegNo = tregno.Text;
                 ListItem lidrivername = new ListItem();
                 lidrivername = ddldriver.SelectedItem;
                 string drivername = string.Empty;
                 if (lidrivername != null)
                 {
                     drivername = lidrivername.Text;
                 }
                 else
                 {
                     NotifyMessages("Select Driver", Common.ErrorType.Error);
                     return;
                 }
                 _objPlslot.Drivername = drivername;
                 _objPlslot.KeyNo = "0";
                 _objPlslot.InOutDateTime = DateTime.Now;
                 _objPlslot.GuidNo = hdnguid.Value;
                 string[] result = _objBlslot.InsertUpdateTransaction(_objPlslot);
                 if (result[1] != "-1")
                 {
                     ClearControls();
                     NotifyMessages(result[0], Common.ErrorType.Information);
                 }
                 else
                 {
                     NotifyMessages(result[0], Common.ErrorType.Error);
                 }
             }
             catch (Exception ex)
             {
                 NotifyMessages(ex.Message, Common.ErrorType.Error);
             }
         }

         private void ClearControls()
         {
             tkeyno.Text = string.Empty;
             tregno.Text = string.Empty;
             trfidno.Text = string.Empty;
             hdnguid.Value = "";
             hdnslotid.Value = "";
             txtslot.Text = string.Empty;
             txtindriver.Text = string.Empty;
             trfidno.Focus();
         }


         public void NotifyMessages(string message, Common.ErrorType et)
         {
             divmsg.Visible = true;
             lblError.Text = message;
             if (et == Common.ErrorType.Error)
             {
                 divmsg.Attributes.Add("class", "alert alert-danger");
             }
             else if (et == Common.ErrorType.Information)
             {
                 divmsg.Attributes.Add("class", "alert alert-success");
             }
         }
    }
}