﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PLCPS;
using BLCPS;
using System.Data;
using CarParkingSystem.Code;
using System.Web.Services;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

namespace CarParkingSystem
{
    public partial class CarIn : PageBase
    {
        PLSlot _objPlslot = new PLSlot();
        BLSlot _objBlslot = new BLSlot();

        PLvariant _objPlvariant = new PLvariant();
        BLvariant _objBlvariant = new BLvariant();

        PLDriver _objPldriver = new PLDriver();
        BLDriver _objBldriver = new BLDriver();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Setheadertextdetails();
                Bindvarints();
                BindSlot();
                trfidno.Focus();
                BindDriver();
            }
        }

        private void BindDriver()
        {
            try
            {
                _objPldriver.driverid = 0;
                DataSet dsdriver = new DataSet();
                _objPldriver.companyid = UserSession.CompanyIDCurrent;
                _objPldriver.locationid = UserSession.LocationIDCurrent;
                _objPldriver.driverid = 0;
                dsdriver = _objBldriver.Getdrivers(_objPldriver);
                if (dsdriver != null && dsdriver.Tables.Count > 0 && dsdriver.Tables[0].Rows.Count > 0)
                {
                    this.ddldriver.DataSource = dsdriver.Tables[0];
                    this.ddldriver.DataTextField = "drivername";
                    this.ddldriver.DataValueField = "driverid";
                    this.ddldriver.DataBind();
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, Common.ErrorType.Error);
            }
        }

        private void Bindvarints()
        {
            DataSet dsVariant = new DataSet();
            _objPlvariant.companyid = UserSession.CompanyIDCurrent;
            _objPlvariant.locationid = UserSession.LocationIDCurrent;
            _objPlvariant.variantid = 0;
            dsVariant = _objBlvariant.Getvariants(_objPlvariant);
            if (dsVariant != null && dsVariant.Tables.Count > 0 && dsVariant.Tables[0].Rows.Count > 0)
            {
                this.ddlvariant.DataSource = dsVariant.Tables[0];
                this.ddlvariant.DataTextField = "varianttype";
                this.ddlvariant.DataValueField = "ID";
                this.ddlvariant.DataBind();
            }
        }

        protected void bSave_Click(object sender, EventArgs e)
        {
            try
            {
                int slotid = 0;
                string rfid = string.Empty;
                if (trfidno.Text == string.Empty)
                {
                    NotifyMessages("Enter RFID No", Common.ErrorType.Error);
                    return;
                }
                if (!Common.DDVal(ddlslot, out slotid))
                {
                    NotifyMessages("Select Slot", Common.ErrorType.Error);
                    return;
                }
                if (tregno.Text == string.Empty)
                {
                    NotifyMessages("Enter Reg.No", Common.ErrorType.Error);
                    return;
                }
                if (tkeyno.Text == string.Empty)
                {
                    NotifyMessages("Enter Key No", Common.ErrorType.Error);
                    return;
                }
                rfid = ddlslot.SelectedItem.Text;
                _objPlslot.Type = "in";
                //_objPlslot.RFIDNo = trfidno.Text.Trim();
                _objPlslot.RFIDNo = rfid.Trim();
                _objPlslot.slotid = slotid;
                _objPlslot.companyid = UserSession.CompanyIDCurrent;
                _objPlslot.locationid = UserSession.LocationIDCurrent;
                _objPlslot.UserId = UserSession.UserID;
                _objPlslot.RegNo = tregno.Text;
                _objPlslot.KeyNo = tkeyno.Text;
                ListItem lidrivername = new ListItem();
                lidrivername = ddldriver.SelectedItem;
                string drivername = string.Empty;
                if (lidrivername != null)
                {
                    drivername = lidrivername.Text;
                }
                else
                {
                    NotifyMessages("Select Driver", Common.ErrorType.Error);
                    return;
                }
                _objPlslot.Drivername = drivername;
                _objPlslot.InOutDateTime = DateTime.Now;
                _objPlslot.GuidNo = Guid.NewGuid().ToString();
                string[] result = _objBlslot.InsertUpdateTransaction(_objPlslot);
                if (result[1] != "-1")
                {
                    BindSlot();
                    ClearControls();
                    NotifyMessages(result[0], Common.ErrorType.Information);
                }
                else
                {
                    NotifyMessages(result[0], Common.ErrorType.Error);
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, Common.ErrorType.Error);
            }
        }

        private void ClearControls()
        {
            tkeyno.Text = string.Empty;
            tregno.Text = string.Empty;
            trfidno.Text = string.Empty;
            trfidno.Focus();
        }


        public void NotifyMessages(string message, Common.ErrorType et)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (et == Common.ErrorType.Error)
            {
                divmsg.Attributes.Add("class", "alert alert-danger");
            }
            else if (et == Common.ErrorType.Information)
            {
                divmsg.Attributes.Add("class", "alert alert-success");
            }
        }

        /// <summary>
        /// Sets the Header and Sub Header based on the Link.
        /// </summary>
        private void Setheadertextdetails()
        {
            try
            {
                string mainmenu = string.Empty;
                string submenu = string.Empty;
                string[] file = Request.CurrentExecutionFilePath.Split('/');
                string fileName = file[file.Length - 1];
                Getmainandsubform(fileName, out mainmenu, out submenu);
                if (mainmenu.Trim() == string.Empty)
                    mainmenu = "Transaction";
                if (submenu.Trim() == string.Empty)
                    submenu = "Car-In";
                lmainheader.Text = mainmenu;
                lsubheader.Text = submenu;
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message.ToString(), Common.ErrorType.Error);
            }
        }

        private void BindSlot()
        {
            try
            {
                int variantid = 0;
                Common.DDVal(ddlvariant, out variantid);
                _objPlslot.VariantId = variantid;
                _objPlslot.slotid = 0;
                DataSet dsSlot = new DataSet();
                _objPlslot.companyid = UserSession.CompanyIDCurrent;
                _objPlslot.locationid = UserSession.LocationIDCurrent;
                _objPlslot.slotid = 0;

                _objPlslot.Type = "free";
                dsSlot = _objBlslot.GetSlotDetailsByVariant(_objPlslot);
                if (dsSlot != null && dsSlot.Tables.Count > 0 && dsSlot.Tables[0].Rows.Count > 0)
                {
                    this.ddlslot.DataSource = dsSlot.Tables[0];
                    this.ddlslot.DataTextField = "slotname";
                    this.ddlslot.DataValueField = "slotid";
                    this.ddlslot.DataBind();
                }
                else
                {
                    this.ddlslot.Items.Clear();
                    tkeyno.Text = string.Empty;
                }

                int slotid = 0;
                if (!Common.DDVal(ddlslot, out slotid))
                {
                    //NotifyMessages("Select Slot", Common.ErrorType.Error);
                    return;
                }
                DataRow[] drslotdet = dsSlot.Tables[0].Select("slotid=" + slotid);
                if (drslotdet.Length > 0)
                {
                    tkeyno.Text = drslotdet[0]["keyno"].ToString();
                }

            }
            catch (Exception ex)
            {
            }
        }

        public class SlotDet
        {
            public string KeyNo { get; set; }
        }

        [WebMethod]
        public static SlotDet[] GetSlotDetails(string slotid)
        {
            try
            {
                List<SlotDet> lstSlot = new List<SlotDet>();
                DataSet dsSlotDet = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("DBCPS");
                DbCommand cmd = db.GetStoredProcCommand("Qry_GetSlotDetails");
                db.AddInParameter(cmd, "@CompanyId", DbType.Int32, UserSession.CompanyIDCurrent);
                db.AddInParameter(cmd, "@LocationId", DbType.Int32, UserSession.LocationIDCurrent);
                db.AddInParameter(cmd, "@SlotId", DbType.Int32, Convert.ToInt32(slotid));
                db.AddInParameter(cmd, "@Type", DbType.String, "all");
                dsSlotDet = db.ExecuteDataSet(cmd);
                foreach (DataRow drEmp in dsSlotDet.Tables[0].Rows)
                {
                    SlotDet objslotdet = new SlotDet();
                    objslotdet.KeyNo = drEmp["keyno"].ToString();
                    lstSlot.Add(objslotdet);
                }
                return lstSlot.ToArray();
            }
            catch (Exception)
            {
                throw;
            }
        }

        protected void ddlvariant_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindSlot();
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, Common.ErrorType.Error);
            }
        }
    }
}