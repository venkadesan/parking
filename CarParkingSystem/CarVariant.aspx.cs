﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CarParkingSystem.Code;
using PLCPS;
using BLCPS;
using System.Data;

namespace CarParkingSystem
{
    public partial class CarVariant : PageBase
    {
        PLvariant _objPlvariant = new PLvariant();
        BLvariant _objBlvariant = new BLvariant();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ButtonStatus(Common.ButtonStatus.Cancel);
                BindSlot();
                Setheadertextdetails();
            }
        }

        /// <summary>
        /// Sets the Header and Sub Header based on the Link.
        /// </summary>
        private void Setheadertextdetails()
        {
            try
            {
                string mainmenu = string.Empty;
                string submenu = string.Empty;
                string[] file = Request.CurrentExecutionFilePath.Split('/');
                string fileName = file[file.Length - 1];
                Getmainandsubform(fileName, out mainmenu, out submenu);
                if (mainmenu.Trim() == string.Empty)
                    mainmenu = "Master";
                if (submenu.Trim() == string.Empty)
                    submenu = "Car-Variant";
                lmainheader.Text = mainmenu;
                lsubheader.Text = submenu;
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message.ToString(), Common.ErrorType.Error);
            }
        }

        protected void bNew_Click(object sender, EventArgs e)
        {
            ButtonStatus(Common.ButtonStatus.New);
        }

        protected void bSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (tcarvariant.Text.Trim() == string.Empty)
                {
                    NotifyMessages("Enter Variant", Common.ErrorType.Error);
                    return;
                }              

                _objPlvariant.varianttye = tcarvariant.Text.Trim();              
                _objPlvariant.variantid = Convert.ToInt32(hdnvariantid.Value);
                _objPlvariant.companyid = UserSession.CompanyIDCurrent;
                _objPlvariant.locationid = UserSession.LocationIDCurrent;
                _objPlvariant.userid = UserSession.UserID;
                string[] result = _objBlvariant.InsertUpdatevariant(_objPlvariant);
                if (result[1] != "-1")
                {
                    BindSlot();
                    ButtonStatus(Common.ButtonStatus.Cancel);
                    NotifyMessages(result[0], Common.ErrorType.Information);
                }
                else
                {
                    NotifyMessages(result[0], Common.ErrorType.Error);
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message.ToString(), Common.ErrorType.Error);
            }
        }

        private void BindSlot()
        {
            try
            {
                _objPlvariant.variantid = 0;
                DataSet dsvariant = new DataSet();
                _objPlvariant.companyid = UserSession.CompanyIDCurrent;
                _objPlvariant.locationid = UserSession.LocationIDCurrent;
                _objPlvariant.variantid = 0;            
                dsvariant = _objBlvariant.Getvariants(_objPlvariant);
                if (dsvariant != null && dsvariant.Tables.Count > 0 && dsvariant.Tables[0].Rows.Count > 0)
                {
                    rptrvariant.DataSource = dsvariant.Tables[0];
                    rptrvariant.DataBind();
                }
                else
                {
                    rptrvariant.DataSource = null;
                    rptrvariant.DataBind();
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message.ToString(), Common.ErrorType.Error);
            }
        }

        protected void bCancel_Click(object sender, EventArgs e)
        {
            ButtonStatus(Common.ButtonStatus.Cancel);
        }

        protected void rptrvariant_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "edit")
                {
                    hdnvariantid.Value = e.CommandArgument.ToString();
                    ButtonStatus(Common.ButtonStatus.Edit);
                    BindControls();
                }
                if (e.CommandName == "delete")
                {
                    int variantid = Convert.ToInt32(e.CommandArgument);
                    _objPlvariant.variantid = variantid;
                    string result = _objBlvariant.Deletevariant(_objPlvariant);
                    NotifyMessages(result, Common.ErrorType.Information);
                    BindSlot();
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message.ToLower(), Common.ErrorType.Error);
            }
        }

        private void BindControls()
        {
            try
            {
                _objPlvariant.variantid = Convert.ToInt32(hdnvariantid.Value);
                _objPlvariant.companyid = UserSession.CompanyIDCurrent;
                _objPlvariant.locationid = UserSession.LocationIDCurrent;
                DataSet dsSlotDet = new DataSet();
                dsSlotDet = _objBlvariant.Getvariants(_objPlvariant);
                if (dsSlotDet != null && dsSlotDet.Tables.Count > 0 && dsSlotDet.Tables[0].Rows.Count > 0)
                {
                    DataRow drExpenseType = dsSlotDet.Tables[0].Rows[0];
                    tcarvariant.Text = drExpenseType["varianttype"].ToString();                 
                }
                else
                {
                    tcarvariant.Text = string.Empty;
                    //tShortName.Text = string.Empty;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void NotifyMessages(string message, Common.ErrorType et)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (et == Common.ErrorType.Error)
            {
                divmsg.Attributes.Add("class", "alert alert-danger");
            }
            else if (et == Common.ErrorType.Information)
            {
                divmsg.Attributes.Add("class", "alert alert-success");
            }
        }

        private void ButtonStatus(Common.ButtonStatus status)
        {
            if (status == Common.ButtonStatus.New || status == Common.ButtonStatus.Cancel ||
                status == Common.ButtonStatus.Edit)
            {
                this.tcarvariant.Text  = string.Empty;
            }
            this.bNew.Visible = false;
            this.divmsg.Visible = false;
            if (status == Common.ButtonStatus.New)
            {
                this.pAdd.Visible = true;
                this.bNew.Enabled = false;
                this.bSave.Enabled = true;
            }
            else if (status == Common.ButtonStatus.View)
            {
                this.pAdd.Visible = true;
                this.bNew.Enabled = false;
                this.bSave.Enabled = true;
                this.tcarvariant.Focus();
            }
            else if (status == Common.ButtonStatus.Edit)
            {
                this.tcarvariant.Focus();
                this.pAdd.Visible = true;

                this.bNew.Enabled = false;
                this.bSave.Enabled = true;
            }
            else if (status == Common.ButtonStatus.Cancel)
            {
                this.pAdd.Visible = false;

                this.bNew.Visible = true;
                this.bNew.Enabled = true;
                this.bSave.Enabled = false;
                hdnvariantid.Value = "0";
            }
        }
    }
}