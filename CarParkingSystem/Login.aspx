﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="CarParkingSystem.Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <title>CPS App</title>
    <meta name="description" content="Student App">
    <meta name="author" content="pixelcave">
    <meta name="robots" content="noindex, nofollow">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <!-- Icons -->
    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
    <link rel="shortcut icon" href="img/favicon.ico">
    <link rel="apple-touch-icon" href="img/icon57.png" sizes="57x57">
    <link rel="apple-touch-icon" href="img/icon72.png" sizes="72x72">
    <link rel="apple-touch-icon" href="img/icon76.png" sizes="76x76">
    <link rel="apple-touch-icon" href="img/icon114.png" sizes="114x114">
    <link rel="apple-touch-icon" href="img/icon120.png" sizes="120x120">
    <link rel="apple-touch-icon" href="img/icon144.png" sizes="144x144">
    <link rel="apple-touch-icon" href="img/icon152.png" sizes="152x152">
    <!-- END Icons -->
    <!-- Stylesheets -->
    <!-- Bootstrap is included in its original form, unaltered -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <!-- Related styles of various javascript plugins -->
    <link rel="stylesheet" href="css/plugins.css">
    <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
    <link rel="stylesheet" href="css/main.css">
    <!-- END Stylesheets -->
    <!-- Modernizr (browser feature detection library) & Respond.js (Enable responsive CSS code on browsers that don't support it, eg IE8) -->
    <script src="js/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>
</head>
<body class="login">
    <form id="form1" runat="server">
    <!-- Login Container -->
    <div id="login-container">
        <div id="login-logo">
            <img src="img/template/minoral-logo.jpg" alt="logo" style="width: 80px;height: 80px;">
        </div>
        <br />
        <div class="row" id="dSuc" runat="server">
            <div class="alert alert-success" id="divSuccess" runat="server">
                <button type="button" class="close" data-dismiss="alert">
                    x
                </button>
                <p>
                    <strong>
                        <asp:Literal ID="msgSuc" runat="server"></asp:Literal>
                    </strong>
                </p>
            </div>
        </div>
        <div class="row" id="dErr" runat="server">
            <div class="alert alert-danger" id="diverror" runat="server">
                <button type="button" class="close" data-dismiss="alert">
                    x</button>
                <p>
                    <strong>
                        <asp:Label ID="msgErr" runat="server" />
                    </strong>
                </p>
            </div>
        </div>
        <!-- Login Buttons -->
        <!-- END Login Buttons -->
        <!-- Login Form -->
        <div class="form-horizontal">
            <div class="form-group">
                <div class="col-xs-12">
                    <div class="input-group">
                        <asp:TextBox ID="txtusername" placeholder="Username.." class="form-control" runat="server"
                            Style="width: 271px;"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-12">
                    <div class="input-group">
                        <asp:TextBox ID="txtpassword" runat="server" placeholder="Password.." class="form-control"
                            TextMode="Password" Style="width: 271px;"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="clearfix">
                <div class="btn-group btn-group-sm pull-right">
                    <asp:Button ID="btnlogin" runat="server" CssClass="btn btn-success" Text="Login"
                        OnClick="btnlogin_Click" />
                </div>
            </div>
        </div>
        <!-- END Login Form -->
    </div>
    <!-- END Login Container -->
    <!-- Include Jquery library from Google's CDN but if something goes wrong get Jquery from local file (Remove 'http:' if you have SSL) -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script>        !window.jQuery && document.write(decodeURI('%3Cscript src="js/vendor/jquery-1.11.1.min.js"%3E%3C/script%3E'));</script>
    <!-- Bootstrap.js -->
    <script src="js/vendor/bootstrap.min.js"></script>
    <!-- Jquery plugins and custom javascript code -->
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>
    <!-- Javascript code only for this page -->
    </form>
</body>
</html>
