﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CarParkingSystem.Code;

using KB = Kowni.BusinessLogic;
using KCB = Kowni.Common.BusinessLogic;

namespace CarParkingSystem
{
    public partial class Site1 : System.Web.UI.MasterPage
    {
        KB.BLRoles.BLRoleCompany objRoleComp = new KB.BLRoles.BLRoleCompany();
        KB.BLRoles.BLRoleLocation objRoleLoc = new KB.BLRoles.BLRoleLocation();
        KB.BLCompany objCompany = new KB.BLCompany();
        KB.BLLocation objLocation = new KB.BLLocation();

        KB.BLUser.BLUserLocation objuserLocation1 = new KB.BLUser.BLUserLocation();
        KB.BLUser.BLUserCompany objuserCompany = new KB.BLUser.BLUserCompany();

        protected void Page_Load(object sender, EventArgs e)
        {
            // this.lblUSerName.Text = UserSession.UserFirstName;
            if (UserSession.UserID == 0)
            {
                Response.Redirect("Login.aspx");
            }
            if (!IsPostBack)
            {
                string versionno = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
                llvno.Text = versionno;

                LoadDDPermittedCompany();
                LoadDDPermittedLocation();
                GetselectedDetails();

                //lnkprimary.Attributes.Add("href", "/css/themes/deepblue.css");
                //lnksecondary.Attributes.Add("href", "/css/themes/navdeepGray.css");

                //SetColorThemes();
            }
        }

        //private void SetColorThemes()
        //{
        //    PLThemeselection objplTheme = new PLThemeselection();
        //    BLThemeSelection objblTheme = new BLThemeSelection();

        //    objplTheme.CompanyID = UserSession.CompanyIDCurrent;
        //    DataSet dsThemes = new DataSet();
        //    dsThemes = objblTheme.GetColorThemes(objplTheme);
        //    if (dsThemes != null && dsThemes.Tables.Count > 0 && dsThemes.Tables[0].Rows.Count > 0)
        //    {
        //        DataRow drthemes = dsThemes.Tables[0].Rows[0];
        //        string primary = drthemes["primaryTheme"].ToString();
        //        string secondary = drthemes["secondaryTheme"].ToString();

        //        if (primary != "default")
        //        {
        //            lnkprimary.Attributes.Add("href", "css/themes/" + primary);
        //        }
        //        lnksecondary.Attributes.Add("href", "css/themes/" + secondary);
        //    }
        //}

        private void LoadDDPermittedCompany()
        {
            DataSet ds = new DataSet();
            //ds = objRoleComp.GetRoleCompany(UserSession.RoleID, UserSession.CompanyIDUser);
            ds = objuserCompany.GetUserCompany(UserSession.UserID);

            DataTable dFiltered = ds.Tables[0].Clone();
            DataRow[] dRowUpdates = ds.Tables[0].Select(" UserCompanyID > 0", "companyname");
            foreach (DataRow dr in dRowUpdates)
                dFiltered.ImportRow(dr);

            this.ddlCompany.DataSource = dFiltered;
            this.ddlCompany.DataTextField = "CompanyName";
            this.ddlCompany.DataValueField = "CompanyID";
            this.ddlCompany.DataBind();

            //KCB.BLMail i2iMail = new KCB.BLMail();

            //i2iMail.AddToUserMail()
            //
            //get companies Country ID
            //
            DataSet dscomp = new DataSet();
            dscomp = objCompany.GetCompany(UserSession.CompanyIDCurrent);
            try
            {
                UserSession.Settings.Images.ShowCompanyImage = Convert.ToBoolean(dscomp.Tables[0].Rows[0]["showimage"]);

                UserSession.GroupID = Convert.ToInt32(dscomp.Tables[0].Rows[0]["groupID"]);
            }
            catch { }

            try
            {
                this.ddlCompany.ClearSelection();
                this.ddlCompany.Items.FindByValue(UserSession.CompanyIDCurrent.ToString()).Selected = true;
            }
            catch
            {
                if (dFiltered.Rows.Count > 0)
                {
                    UserSession.CompanyIDCurrent = Convert.ToInt32(this.ddlCompany.Items[0].Value);
                    UserSession.CompanyName = this.ddlCompany.Items[0].Text;
                }
            }
        }

        private void LoadDDPermittedLocation()
        {
            DataSet ds = new DataSet();
            //ds = objuserLocation1.GetUserLocation(UserSession.RoleID, UserSession.CompanyIDCurrent);
            ds = objuserLocation1.GetUserLocation(UserSession.UserID, UserSession.CompanyIDCurrent);

            DataTable dFiltered = ds.Tables[0].Clone();
            //DataTable dFiltered = ds;
            DataRow[] dRowUpdates = ds.Tables[0].Select(" UserLocationID > 0", "locationname");
            foreach (DataRow dr in dRowUpdates)
                dFiltered.ImportRow(dr);

            this.ddlLocation.DataSource = dFiltered;
            this.ddlLocation.DataTextField = "locationname";
            this.ddlLocation.DataValueField = "locationid";
            this.ddlLocation.DataBind();

            try
            {
                this.ddlLocation.ClearSelection();
                this.ddlLocation.Items.FindByValue(UserSession.LocationIDCurrent.ToString()).Selected = true;
            }
            catch
            {
                UserSession.LocationIDCurrent = 0;
                UserSession.LocationName = string.Empty;

                if (dFiltered.Rows.Count > 0)
                {
                    UserSession.LocationIDCurrent = Convert.ToInt32(this.ddlLocation.Items[0].Value);
                    UserSession.LocationName = this.ddlLocation.Items[0].Text;
                }
            }
        }

        protected void GetselectedDetails()
        {
            try
            {
                int CompanyIDCurrent = 0;
                Common.DDVal(ddlCompany, out CompanyIDCurrent);
                int LocationIDCurrent = 0;
                Common.DDVal(ddlLocation, out LocationIDCurrent);
                UserSession.CompanyIDCurrent = Convert.ToInt32(CompanyIDCurrent);
                UserSession.LocationIDCurrent = Convert.ToInt32(LocationIDCurrent);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                UserSession.CompanyIDCurrent = Convert.ToInt32(this.ddlCompany.SelectedItem.Value);
                UserSession.CompanyName = this.ddlCompany.SelectedItem.Text.Trim();

                this.ddlCompany.ClearSelection();
                this.ddlCompany.Items.FindByValue(UserSession.CompanyIDCurrent.ToString()).Selected = true;
                LoadDDPermittedLocation();
                //
                //get companies Country ID
                //
                DataSet ds = new DataSet();
                ds = objCompany.GetCompany(UserSession.CompanyIDCurrent);
                try
                {
                    UserSession.Settings.Images.ShowCompanyImage = Convert.ToBoolean(ds.Tables[0].Rows[0]["showimage"]);

                    UserSession.GroupID = Convert.ToInt32(ds.Tables[0].Rows[0]["groupID"]);
                }
                catch { }

                Response.Redirect(Request.RawUrl);
            }
            catch { }
        }

        protected void ddlLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                UserSession.LocationIDCurrent = Convert.ToInt32(this.ddlLocation.SelectedItem.Value);
                UserSession.LocationName = this.ddlLocation.SelectedItem.Text.Trim();

                this.ddlLocation.ClearSelection();
                this.ddlLocation.Items.FindByValue(UserSession.LocationIDCurrent.ToString()).Selected = true;

                Response.Redirect(Request.RawUrl);
            }
            catch { }
        }

        public void VerifyRenderingInServerForm(Control control)
        {

        }

        protected void lnklogout_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            Session.Clear();
            bool isclear = UserSession.ClearSession;
            Response.Redirect("Login.aspx");

        }
    }
}