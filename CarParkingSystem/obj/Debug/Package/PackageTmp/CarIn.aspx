﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true"
    CodeBehind="CarIn.aspx.cs" Inherits="CarParkingSystem.CarIn" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content" runat="server">
    <style>
        .upper-case
        {
            text-transform: uppercase;
        }
    </style>
    <script type="text/javascript">
        function Check(textBox, maxLength) {

            if (textBox.value.length > maxLength) {
                textBox.value = textBox.value.substr(0, maxLength);
            }
        }
    </script>
    <script type="text/javascript">

        $(document).ready(function () {
            $('#carin').addClass("active");
            //$("#lidepartment").parent().parent().removeClass("dropdown a1");
            $("#carin").parent().parent().addClass("dropdown a1 open");

            $("#<%= trfidno.ClientID%>").keyup(function (e) {

                if (e.keyCode == 13) {
                    $("#<%= trfidno.ClientID%>").val($("#<%= trfidno.ClientID%>").val().trim());
                    var rfidno = $("#<%= trfidno.ClientID%>").val();

                    var parts = rfidno.split("-");
                    rfidno = parts[1].trim();

                    $("#<%= trfidno.ClientID%>").val(rfidno);

                    var isexists = 0;
                    $("#<%= ddlslot.ClientID%> option").each(function () {
                        if ($(this).text().toLowerCase() == rfidno.toLowerCase()) {
                            //alert($(this).text() + ' ' + rfidno + ' ' + $(this).val());
                            $("#<%= ddlslot.ClientID%>").val($(this).val());
                            $("#<%= ddlslot.ClientID%>").trigger('chosen:updated');
                            //$(this).attr('selected', 'selected');
                            onSlotChange();
                            isexists = 1;
                        }
                    });
                    if (isexists == 0) {
                        alert('RFID Not Found');
                        $("#<%= trfidno.ClientID%>").val('');
                        $("#<%= tkeyno.ClientID%>").val('');
                    }

                }
            });
        });

        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }

        function onSlotChange() {

            var slotid = $("#<%= ddlslot.ClientID%>").val();

            if (slotid != '') {
                $.ajax({
                    type: "POST",
                    url: "CarIn.aspx/GetSlotDetails",
                    data: "{ 'slotid': '" + slotid
                       + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        if (data.d.length == 0) {
                            alert('No Data Found');
                        }
                        for (var i = 0; i < data.d.length; i++) {
                            var KeyNo = data.d[i].KeyNo;

                            $("#<%= tkeyno.ClientID%>").val(KeyNo);
                            $("#<%= tregno.ClientID%>").focus();
                        }
                    },
                    failure: function (msg) {
                        alert(msg);
                    }
                });
            }
            else {
                alert('select Slot');
            }
        }
        
    </script>
    <ul id="nav-info" class="clearfix">
        <li>
            <asp:Literal ID="lmainheader" runat="server" Text="Transaction"></asp:Literal></li>
        <li>
            <asp:Literal ID="lsubheader" runat="server" Text="CarIn"></asp:Literal></li>
    </ul>
    <!-- /submenu -->
    <!-- content main container -->
    <div class="alert alert-danger" id="divmsg" visible="false" runat="server">
        <button type="button" class="close" data-dismiss="alert">
            ×</button>
        <asp:Label runat="server" ID="lblError"></asp:Label>
    </div>
    <div class="form-horizontal form-box">
        <h4 class="form-box-header">
            Car-In</h4>
        <div class="form-box-content">
            <asp:Panel runat="server" ID="pAdd">
                <div class="form-group">
                    <label class="control-label col-md-2">
                        <asp:Label Text="Car-Variant" runat="server" ID="Label5" />:</label>
                    <div class="col-md-6">
                        <asp:DropDownList ID="ddlvariant" runat="server" class="form-control select-chosen"
                            AutoPostBack="True" OnSelectedIndexChanged="ddlvariant_SelectedIndexChanged">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2">
                        <asp:Label Text="RFID No" runat="server" ID="lStatusName" />:</label>
                    <div class="col-md-6">
                        <asp:TextBox ID="trfidno" Font-Bold="true" Font-Size="Large" TextMode="MultiLine"
                            Rows="3" runat="server" ToolTip="Please enter the RFID No" class="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2">
                        <asp:Label Text="Slot" runat="server" ID="Label3" />:</label>
                    <div class="col-md-6">
                        <asp:DropDownList ID="ddlslot" onchange="onSlotChange()" runat="server" class="form-control select-chosen">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2">
                        <asp:Label Text="Key No" runat="server" ID="Label1" />:</label>
                    <div class="col-md-6">
                        <asp:TextBox ID="tkeyno" runat="server" Font-Bold="true" Font-Size="Large" TextMode="MultiLine"
                            Rows="3" onpaste="return false" ToolTip="Please enter the Key No" class="form-control"></asp:TextBox><%--onkeypress="return isNumber(event)"--%>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2">
                        <asp:Label Text="Reg No." runat="server" ID="Label2" />:</label>
                    <div class="col-md-6">
                        <asp:TextBox ID="tregno" runat="server" Font-Bold="true" Font-Size="Large" TextMode="MultiLine"
                            onKeyUp="javascript:Check(this, 10);" Rows="3" ToolTip="Please enter the Reg.No"
                            class="form-control upper-case"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2">
                        <asp:Label Text="Driver Name" runat="server" ID="Label4" />:</label>
                    <div class="col-md-6">
                        <%-- <asp:TextBox ID="txtdrivername" runat="server" class="form-control"></asp:TextBox>--%>
                        <asp:DropDownList ID="ddldriver" class="form-control select-chosen" runat="server">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form-group form-actions">
                    <div class="col-md-10 col-md-offset-2">
                        <asp:Button ID="bSave" Text="In" runat="server" ToolTip="Click button to save record"
                            TabIndex="3" class="btn btn-success" OnClick="bSave_Click" />
                        <asp:HiddenField ID="hdnguid" Value="0" runat="server" />
                    </div>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
