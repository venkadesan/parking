﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true"
    CodeBehind="VariantSlotMapping.aspx.cs" Inherits="CarParkingSystem.VariantSlotMapping" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content" runat="server">
    <ul id="nav-info" class="clearfix">
        <li>
            <asp:Literal ID="lmainheader" runat="server" Text="Master"></asp:Literal></li>
        <li>
            <asp:Literal ID="lsubheader" runat="server" Text="Vehicle Variant - Slot Mapping"></asp:Literal></li>
    </ul>
    <!-- /submenu -->
    <!-- content main container -->
    <div class="alert alert-danger" id="divmsg" visible="false" runat="server">
        <button type="button" class="close" data-dismiss="alert">
            ×</button>
        <asp:Label runat="server" ID="lblError"></asp:Label>
    </div>
    <div class="form-horizontal form-box">
        <h4 class="form-box-header">
            Vehicle Variant - Slot Mapping</h4>
        <div class="form-box-content">
            <asp:Panel runat="server" ID="pAdd">
                <div class="form-group">
                    <label class="control-label col-md-2">
                        <asp:Label Text="Vehicle Variant" runat="server" ID="Label4" />:</label>
                    <div class="col-md-3">
                        <asp:DropDownList ID="ddlvariant" runat="server" class="form-control select-chosen"
                            AutoPostBack="True" OnSelectedIndexChanged="ddlvariant_SelectedIndexChanged">
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-3">
                        <div class="input-group">
                            <asp:Label Text="Parking Type" runat="server" ID="Label5" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <asp:DropDownList ID="ddltype" runat="server" class="form-control select-chosen"
                            AutoPostBack="True" OnSelectedIndexChanged="ddltype_SelectedIndexChanged">
                            <asp:ListItem Value="1" Text="Static"></asp:ListItem>
                            <asp:ListItem Value="2" Text="Puzzle"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2">
                        <asp:Label Text="Slots" runat="server" ID="Label3" /></label>
                    <div class="col-md-10">
                        <asp:ListBox ID="lstslots" CssClass="form-control select-select2" SelectionMode="Multiple"
                            runat="server"></asp:ListBox>
                    </div>
                </div>
            </asp:Panel>
            <h4 class="form-box-header">
                Mapped Slots</h4>
            <div class="row">
                <table id="example-datatables" class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr class="submenu new-color">
                            <th>
                                S.No
                            </th>
                            <th>
                                Slot
                            </th>
                            <th style="width: 10%; text-align: center;">
                                Action
                            </th>
                        </tr>
                    </thead>
                    <asp:Repeater ID="rptrslot" runat="server">
                        <HeaderTemplate>
                            <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <asp:Label ID="Label2" runat="server" Text='<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>' />
                                </td>
                                <td>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("slotname") %>' ToolTip='<%# Eval("slotname") %>' />
                                </td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <asp:CheckBox ID="chkslot" Checked='<%# Eval("checked") %>' runat="server" />
                                        <asp:HiddenField ID="hdnslotid" Value='<%# Eval("slotid") %>' runat="server" />
                                    </div>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody> </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </table>
            </div>
            <div class="form-group form-actions">
                <div class="col-md-10 col-md-offset-2">
                    <asp:Button ID="bSave" Text="Save" runat="server" ToolTip="Click button to save record"
                        TabIndex="3" class="btn btn-success" onclick="bSave_Click"  />                 
                </div>
            </div>
        </div>
    </div>
</asp:Content>
