﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true"
    CodeBehind="CodeGen.aspx.cs" Inherits="CarParkingSystem.CodeGen" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content" runat="server">
    <ul id="nav-info" class="clearfix">
        <li>
            <asp:Literal ID="lmainheader" runat="server" Text="Master"></asp:Literal></li>
        <li>
            <asp:Literal ID="lsubheader" runat="server" Text="QR-Code"></asp:Literal></li>
        <li>
            <asp:LinkButton ID="bdownload" runat="server" ToolTip="Click button to download QR Code"
                class="fa fa-download" OnClick="bdownload_Click" />
        </li>
    </ul>
</asp:Content>
