﻿var webApp = function () {
    /*
    * Color Themes
    */
    var colorList = $('.theme-colors');
    var themeLink = $('#theme-link');
    var theme;

    if (themeLink.length) {
        theme = themeLink.attr('href');

        $('li', colorList).removeClass('active');
        $('a[data-theme="' + theme + '"]', colorList).parent('li').addClass('active');
    }

    $('a', colorList).click(function (e) {
        // Get theme name
        theme = $(this).data('theme');

        alert(theme);

        $('li', colorList).removeClass('active');
        $(this).parent('li').addClass('active');

        if (theme === 'default') {
            if (themeLink.length) {
                themeLink.remove();
                themeLink = $('#theme-link');
            }
        } else {
            if (themeLink.length) {
                themeLink.attr('href', theme);
            } else {
                $('link[href="css/main.css"]').after('<link id="theme-link" rel="stylesheet" href="' + theme + '">');
                themeLink = $('#theme-link');
            }
        }
    });
};