﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true"
    CodeBehind="CarOut.aspx.cs" Inherits="CarParkingSystem.CarOut" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content" runat="server">
    <style>
        .upper-case
        {
            text-transform: uppercase;
        }
    </style>
    <script type="text/javascript">

        $(document).ready(function () {
            $('#carout').addClass("active");
            //$("#lidepartment").parent().parent().removeClass("dropdown a1");
            $("#carout").parent().parent().addClass("dropdown a1 open");


            $("#<%= trfidno.ClientID%>").keyup(function (e) {
                if (e.keyCode == 13) {                   
                    GetTransaction();
                }
            });


        });

        function GetTransaction() {
            $("#<%= trfidno.ClientID%>").val($("#<%= trfidno.ClientID%>").val().trim());
            var rfidno = $("#<%= trfidno.ClientID%>").val();
            var parts = rfidno.split("-");
            rfidno = parts[1].trim();
            $("#<%= trfidno.ClientID%>").val(rfidno);
            if (rfidno != '') {
                $.ajax({
                    type: "POST",
                    url: "CarOut.aspx/GetTransactionDetails",
                    data: "{ 'rfidno': '" + rfidno
                       + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        if (data.d.length == 0) {
                            alert('No Data Found');
                            $('#<%=hdnguid.ClientID %>').val('');
                            $('#<%=hdnslotid.ClientID %>').val(0);
                            $("#<%= txtslot.ClientID%>").val('');
                            $("#<%= tkeyno.ClientID%>").val('');
                            $("#<%= tregno.ClientID%>").val('');
                            $("#<%= trfidno.ClientID%>").val('');

                            $("#<%= txtindriver.ClientID%>").val('');

                        }
                        for (var i = 0; i < data.d.length; i++) {
                            var slotname = data.d[i].slotname;
                            var KeyNo = data.d[i].KeyNo;
                            var RegNo = data.d[i].RegNo;
                            var Guid = data.d[i].Guid;
                            var SlotId = data.d[i].SlotId;
                            var indriver = data.d[i].InDrivername;

                            $('#<%=hdnguid.ClientID %>').val(Guid);
                            $('#<%=hdnslotid.ClientID %>').val(SlotId);
                            $("#<%= txtslot.ClientID%>").val(slotname);
                            $("#<%= tkeyno.ClientID%>").val(KeyNo);
                            $("#<%= tregno.ClientID%>").val(RegNo);
                            $("#<%= txtindriver.ClientID%>").val(indriver);


                            $("#<%= ddldriver.ClientID%>").focus();
                        }
                    },
                    failure: function (msg) {
                        alert(msg);
                    }
                });
            }
            else {
                alert('Enter RFID No');
                $('#<%=hdnguid.ClientID %>').val('');
                $('#<%=hdnslotid.ClientID %>').val(0);
                $("#<%= txtslot.ClientID%>").val('');
                $("#<%= tkeyno.ClientID%>").val('');
                $("#<%= tregno.ClientID%>").val('');
                $("#<%= txtindriver.ClientID%>").val('');


                $("#<%= trfidno.ClientID%>").focus();
            }
        }
        
    </script>
    <script>
        function OnSlotChange() {
            GetTransaction();
        }
    </script>
    <ul id="nav-info" class="clearfix">
        <li>
            <asp:Literal ID="lmainheader" runat="server" Text="Transaction"></asp:Literal></li>
        <li>
            <asp:Literal ID="lsubheader" runat="server" Text="CarIn"></asp:Literal></li>
    </ul>
    <!-- /submenu -->
    <!-- content main container -->
    <div class="alert alert-danger" id="divmsg" visible="false" runat="server">
        <button type="button" class="close" data-dismiss="alert">
            ×</button>
        <asp:Label runat="server" ID="lblError"></asp:Label>
    </div>
    <div class="form-horizontal form-box">
        <h4 class="form-box-header">
            Car-Out</h4>
        <div class="form-box-content">
            <asp:Panel runat="server" ID="pAdd">
                <div class="form-group">
                    <label class="control-label col-md-2">
                        <asp:Label Text="RFID No" runat="server" ID="lStatusName" />:</label>
                    <div class="col-md-6">
                        <div class="input-group">
                            <asp:TextBox ID="trfidno" ont-Bold="true" Font-Size="Large" TextMode="MultiLine"
                                onchange="GetTransaction()" Rows="3" runat="server" ToolTip="Please enter the RFID No"
                                class="form-control"></asp:TextBox>
                            <span class="input-group-btn">
                                <button class="btn btn-default" onclick="GetTransaction()" type="button" id="search">
                                    <i class="gemicon-big-search"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2">
                        <asp:Label Text="Slot" runat="server" ID="Label3" />:</label>
                    <div class="col-md-6">
                        <asp:TextBox ID="txtslot" ont-Bold="true" Font-Size="Large" TextMode="MultiLine"
                            Rows="3" runat="server" class="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2">
                        <asp:Label Text="Key No" runat="server" ID="Label1" />:</label>
                    <div class="col-md-6">
                        <asp:TextBox ID="tkeyno" ont-Bold="true" Font-Size="Large" TextMode="MultiLine" Rows="3"
                            runat="server" class="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2">
                        <asp:Label Text="Reg No." runat="server" ID="Label2" />:</label>
                    <div class="col-md-6">
                        <asp:TextBox ID="tregno" ont-Bold="true" Font-Size="Large" TextMode="MultiLine" Rows="3"
                            MaxLength="10" runat="server" class="form-control  upper-case"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2">
                        <asp:Label Text="In-Driver Name" runat="server" ID="Label5" />:</label>
                    <div class="col-md-6">
                        <asp:TextBox ID="txtindriver" runat="server" ReadOnly="true" class="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2">
                        <asp:Label Text="Driver Name" runat="server" ID="Label4" />:</label>
                    <div class="col-md-6">
                        <%-- <asp:TextBox ID="txtdrivername" runat="server" class="form-control"></asp:TextBox>--%>
                        <asp:DropDownList ID="ddldriver" class="form-control select-chosen" runat="server">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form-group form-actions">
                    <div class="col-md-10 col-md-offset-2">
                        <asp:Button ID="bSave" Text="Out" runat="server" ToolTip="Click button to save record"
                            OnClick="bSave_Click" TabIndex="3" class="btn btn-success" />
                        <asp:HiddenField ID="hdnguid" Value="0" runat="server" />
                        <asp:HiddenField ID="hdnslotid" Value="0" runat="server" />
                    </div>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
