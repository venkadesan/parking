﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CarParkingSystem.Code;
using PLCPS;
using BLCPS;
using System.Data;


namespace CarParkingSystem
{
    public partial class Driver : PageBase
    {
        PLDriver _objPldriver = new PLDriver();
        BLDriver _objBldriver = new BLDriver();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ButtonStatus(Common.ButtonStatus.Cancel);
                BindSlot();
                Setheadertextdetails();
            }
        }

        /// <summary>
        /// Sets the Header and Sub Header based on the Link.
        /// </summary>
        private void Setheadertextdetails()
        {
            try
            {
                string mainmenu = string.Empty;
                string submenu = string.Empty;
                string[] file = Request.CurrentExecutionFilePath.Split('/');
                string fileName = file[file.Length - 1];
                Getmainandsubform(fileName, out mainmenu, out submenu);
                if (mainmenu.Trim() == string.Empty)
                    mainmenu = "Master";
                if (submenu.Trim() == string.Empty)
                    submenu = "Driver";
                lmainheader.Text = mainmenu;
                lsubheader.Text = submenu;
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message.ToString(), Common.ErrorType.Error);
            }
        }

        protected void bNew_Click(object sender, EventArgs e)
        {
            ButtonStatus(Common.ButtonStatus.New);
        }

        protected void bSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (tdriver.Text.Trim() == string.Empty)
                {
                    NotifyMessages("Enter Driver", Common.ErrorType.Error);
                    return;
                }

                _objPldriver.drivername = tdriver.Text.Trim();
                _objPldriver.driverid = Convert.ToInt32(hdndriverid.Value);
                _objPldriver.companyid = UserSession.CompanyIDCurrent;
                _objPldriver.locationid = UserSession.LocationIDCurrent;
                _objPldriver.userid = UserSession.UserID;
                string[] result = _objBldriver.InsertUpdatedriver(_objPldriver);
                if (result[1] != "-1")
                {
                    BindSlot();
                    ButtonStatus(Common.ButtonStatus.Cancel);
                    NotifyMessages(result[0], Common.ErrorType.Information);
                }
                else
                {
                    NotifyMessages(result[0], Common.ErrorType.Error);
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message.ToString(), Common.ErrorType.Error);
            }
        }

        private void BindSlot()
        {
            try
            {
                _objPldriver.driverid = 0;
                DataSet dsdriver = new DataSet();
                _objPldriver.companyid = UserSession.CompanyIDCurrent;
                _objPldriver.locationid = UserSession.LocationIDCurrent;
                _objPldriver.driverid = 0;
                dsdriver = _objBldriver.Getdrivers(_objPldriver);
                if (dsdriver != null && dsdriver.Tables.Count > 0 && dsdriver.Tables[0].Rows.Count > 0)
                {
                    rptrdriver.DataSource = dsdriver.Tables[0];
                    rptrdriver.DataBind();
                }
                else
                {
                    rptrdriver.DataSource = null;
                    rptrdriver.DataBind();
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message.ToString(), Common.ErrorType.Error);
            }
        }

        protected void bCancel_Click(object sender, EventArgs e)
        {
            ButtonStatus(Common.ButtonStatus.Cancel);
        }

        protected void rptrdriver_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "edit")
                {
                    hdndriverid.Value = e.CommandArgument.ToString();
                    ButtonStatus(Common.ButtonStatus.Edit);
                    BindControls();
                }
                if (e.CommandName == "delete")
                {
                    int driverid = Convert.ToInt32(e.CommandArgument);
                    _objPldriver.driverid = driverid;
                    string result = _objBldriver.Deletedriver(_objPldriver);
                    NotifyMessages(result, Common.ErrorType.Information);
                    BindSlot();
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message.ToLower(), Common.ErrorType.Error);
            }
        }

        private void BindControls()
        {
            try
            {
                _objPldriver.driverid = Convert.ToInt32(hdndriverid.Value);
                _objPldriver.companyid = UserSession.CompanyIDCurrent;
                _objPldriver.locationid = UserSession.LocationIDCurrent;
                DataSet dsDriverdet = new DataSet();
                dsDriverdet = _objBldriver.Getdrivers(_objPldriver);
                if (dsDriverdet != null && dsDriverdet.Tables.Count > 0 && dsDriverdet.Tables[0].Rows.Count > 0)
                {
                    DataRow drDriver = dsDriverdet.Tables[0].Rows[0];
                    tdriver.Text = drDriver["drivername"].ToString();
                }
                else
                {
                    tdriver.Text = string.Empty;
                    //tShortName.Text = string.Empty;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void NotifyMessages(string message, Common.ErrorType et)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (et == Common.ErrorType.Error)
            {
                divmsg.Attributes.Add("class", "alert alert-danger");
            }
            else if (et == Common.ErrorType.Information)
            {
                divmsg.Attributes.Add("class", "alert alert-success");
            }
        }

        private void ButtonStatus(Common.ButtonStatus status)
        {
            if (status == Common.ButtonStatus.New || status == Common.ButtonStatus.Cancel ||
                status == Common.ButtonStatus.Edit)
            {
                this.tdriver.Text = string.Empty;
            }
            this.bNew.Visible = false;
            this.divmsg.Visible = false;
            if (status == Common.ButtonStatus.New)
            {
                this.pAdd.Visible = true;
                this.bNew.Enabled = false;
                this.bSave.Enabled = true;
            }
            else if (status == Common.ButtonStatus.View)
            {
                this.pAdd.Visible = true;
                this.bNew.Enabled = false;
                this.bSave.Enabled = true;
                this.tdriver.Focus();
            }
            else if (status == Common.ButtonStatus.Edit)
            {
                this.tdriver.Focus();
                this.pAdd.Visible = true;

                this.bNew.Enabled = false;
                this.bSave.Enabled = true;
            }
            else if (status == Common.ButtonStatus.Cancel)
            {
                this.pAdd.Visible = false;

                this.bNew.Visible = true;
                this.bNew.Enabled = true;
                this.bSave.Enabled = false;
                hdndriverid.Value = "0";
            }
        }
    }
}