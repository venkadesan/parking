﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTextSharp.text;
using iTextSharp.text.pdf;
using QRCoder;
using System.Drawing;
using System.IO;
using CarParkingSystem.Code;
using BLCPS;
using PLCPS;
using System.Data;

namespace CarParkingSystem
{
    public partial class CodeGen : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

            }
        }

        private void GnerateBarcode()
        {

            //iTextSharp.text.Image pic = iTextSharp.text.Image.GetInstance(

            iTextSharp.text.Document doc = new Document(new iTextSharp.text.Rectangle(10, 15), 1, 1, 1, 1);
            //PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "/codes.pdf", FileMode.Create));
            iTextSharp.text.pdf.PdfWriter writer = PdfWriter.GetInstance(doc, Response.OutputStream);
            doc.Open();
            System.Drawing.Image img1 = null;

            PLSlot objplSlot = new PLSlot();

            objplSlot.locationid = UserSession.LocationIDCurrent;

            DataSet ds = new BLSlot().GetBarcode(objplSlot);



            //for (int i = 1; i <= 2; i++)
            //{
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                doc.NewPage();
                iTextSharp.text.pdf.PdfContentByte cb1 = writer.DirectContent;
                //iTextSharp.text.pdf.PdfContentByte cb11 = writer.DirectContent;
                iTextSharp.text.pdf.BaseFont bf = BaseFont.CreateFont(BaseFont.TIMES_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                PdfContentByte cb2 = writer.DirectContent;
                PdfContentByte cb22 = writer.DirectContent;
                PdfContentByte cb3 = writer.DirectContent;
                PdfContentByte cb33 = writer.DirectContent;
                BaseFont bf1 = BaseFont.CreateFont(BaseFont.TIMES_BOLDITALIC, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

                //slotname, FloorName
                string code = row["slotname"].ToString();
                //code = code.ToString("D4");
                QRCodeGenerator qrGenerator = new QRCodeGenerator();
                QRCodeGenerator.QRCode qrCode = qrGenerator.CreateQrCode(UserSession.LocationIDCurrent + "-" + code, QRCodeGenerator.ECCLevel.Q);
                
                using (Bitmap bitMap = qrCode.GetGraphic(20))
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        bitMap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                        byte[] byteImage = ms.ToArray();

                        //iTextSharp.text.Image pic = iTextSharp.text.Image.GetInstance(byteImage);
                        Uri link = new Uri("https://chart.googleapis.com/chart?cht=qr&chl=" + UserSession.LocationIDCurrent + "-" + code + "&choe=UTF-8&chs=100x100");
                        iTextSharp.text.Image pic = iTextSharp.text.Image.GetInstance(link);
                        pic.ScaleToFit(103, 10);
                        pic.SetAbsolutePosition(0.1f, 2.5f);

                        //iTextSharp.text.Image pic = iTextSharp.text.Image.GetInstance(byteImage);
                        //pic.ScaleToFit(80,5);
                        //pic.SetAbsolutePosition(0.1f, 2.5f);

                        cb1.SetFontAndSize(bf, 0.7f);
                        cb1.BeginText();
                        cb1.SetTextMatrix(1.1f, 3f);
                        cb1.ShowText(UserSession.LocationIDCurrent + "-" + code);
                        cb1.EndText();

                        cb1.SetFontAndSize(bf, 1.3f);
                        cb1.BeginText();
                        cb1.SetTextMatrix(0.5f, 11.7f);
                        cb1.ShowText(row["CompanyName"].ToString());
                        cb1.EndText();

                        cb1.SetFontAndSize(bf, 0.9f);
                        cb1.BeginText();
                        cb1.SetTextMatrix(0.5f, 2f);
                        cb1.ShowText("B3" + " - " + code);
                        cb1.EndText();

                        pic.Border = iTextSharp.text.Rectangle.BOX;
                        pic.BorderColor = iTextSharp.text.BaseColor.BLACK;

                        doc.Add(pic);

                        pic.Border = iTextSharp.text.Rectangle.BOX;
                        pic.BorderColor = iTextSharp.text.BaseColor.BLACK;

                        doc.Add(pic);

                    }
                }
            }

            doc.Close();
            Response.Buffer = true;
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=Print Label.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Write(doc);
            Response.End();
        }

        protected void bdownload_Click(object sender, EventArgs e)
        {
            try
            {
                GnerateBarcode();
            }
            catch (Exception ex)
            {
            }
        }
    }
}