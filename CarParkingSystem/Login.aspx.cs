﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Kowni.BusinessLogic;
using Newtonsoft.Json;
using MCB = Kowni.Common.BusinessLogic;
using Kowni.Helper;
using System.Data;
using System.Web.Security;
using CarParkingSystem.Code;
namespace CarParkingSystem
{
    public partial class Login : System.Web.UI.Page
    {
        MCB.BLSingleSignOn objSingleSignOn = new MCB.BLSingleSignOn();
        MCB.BLAnnouncement objAnnouncement = new MCB.BLAnnouncement();
        BLUser objUser = new BLUser();

        protected void Page_Load(object sender, EventArgs e)
        {
           
            Clearmsg();
            // divMessage = (HtmlGenericControl)this.FindControl("divmsg");
            // divMessage.InnerHtml = "&nbsp;";

            //this.lFYear.Text = DateTime.Now.Year.ToString();
            string hashPassword = SimpleHash.ComputeHash("hackersafe", SimpleHash.Algorithm.SHA512, null, "$i2iHackersafe123$MasterMind.InCom$");

            //if (!this.Page.User.Identity.IsAuthenticated)
            //{
            //    FormsAuthentication.RedirectToLoginPage();
            //}
            if (!IsPostBack)
            {
                Session.Clear();
                //string IPAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].Trim();
                //string IPAddress2 = Request.ServerVariables["REMOTE_ADDR"].Trim();
                // this.txtUserName.Focus();
            }
        }
        public void Clearmsg()
        {
            try
            {
                dSuc.Visible = false;
                dErr.Visible = false;
                divSuccess.Visible = false;
                diverror.Visible = false;
                msgSuc.Text = string.Empty;
                msgErr.Text = string.Empty;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public void successmsg(string msg)
        {
            try
            {
                Clearmsg();
                dSuc.Visible = true;
                divSuccess.Visible = true;
                msgSuc.Text = msg;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public void errormsg(string msg)
        {
            try
            {
                Clearmsg();
                dErr.Visible = true;
                diverror.Visible = true;
                msgErr.Text = msg;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        #region Check Login


        protected void btnlogin_Click(object sender, EventArgs e)
        {
            try
            {
                Common.Decode("MTIzNDU=");
                Common.Decryption("MTIzNDU=");
                Clearmsg();

                if (txtusername.Text == string.Empty)
                {
                    errormsg("Enter Username");
                    // lblError.Text = "Enter Username";
                    return;
                }
                if (txtpassword.Text == string.Empty)
                {
                    errormsg("Enter Password");
                    //lblError.Text = "Enter Password";
                    return;
                }

                string _res = string.Empty;
                if (txtusername.Text != string.Empty && txtpassword.Text != string.Empty)
                {
                    int RoleID = 0;
                    int UserID = 0;
                    int CompanyID = 0;
                    string CompanyName = string.Empty;
                    int LocationID = 0;
                    string LocationName = string.Empty;
                    int GroupID = 0;
                    int LanguageID = 0;
                    string UserFName = string.Empty;
                    string UserLName = string.Empty;
                    string UserMailID = string.Empty;
                    string ThemeFolderPath = string.Empty;

                    int toolId = Convert.ToInt32(ConfigurationManager.AppSettings.Get("toolid"));

                    if (!objSingleSignOn.Login(this.txtusername.Text, this.txtpassword.Text, toolId, out RoleID, out UserID, out CompanyID, out CompanyName, out LocationID, out LocationName, out GroupID, out LanguageID, out UserFName, out UserLName, out UserMailID, out ThemeFolderPath))
                    {
                        //_res = JsonConvert.SerializeObject(new Model.StatusData() { Message = "Unauthorized Access.", Status = false }, Newtonsoft.Json.Formatting.Indented);

                    }
                    else
                    {
                        UserSession.FromFramework = false;

                        UserSession.CountryID = 1;
                        UserSession.CountryIDCurrent = 1;
                        UserSession.RoleID = RoleID;
                        UserSession.UserID = UserID;
                        UserSession.CompanyIDUser = CompanyID;
                        UserSession.LocationIDUser = LocationID;

                        UserSession.CompanyName = CompanyName;
                        UserSession.CompanyIDCurrent = CompanyID;

                        UserSession.LocationName = LocationName;
                        UserSession.LocationIDCurrent = LocationID;

                        UserSession.GroupID = GroupID;
                        UserSession.GroupIDCurrent = GroupID;
                        UserSession.LanguageID = LanguageID;
                        UserSession.UserFirstName = UserFName;
                        UserSession.UserLastName = UserLName;
                        UserSession.ThemeFolderPath = ThemeFolderPath;

                        DataSet dsAnn = new DataSet();
                        dsAnn = objAnnouncement.GetAnnouncement(UserSession.CompanyIDCurrent, Convert.ToInt32(Kowni.Common.BusinessLogic.BLMenu.ToolID.MasterFramework));
                        if (dsAnn.Tables.Count > 0 && dsAnn.Tables[0].Rows.Count > 0)
                        {
                            if (dsAnn.Tables[0].Rows[0]["AnnouncementType"].ToString() == "1")
                            {
                                UserSession.Announcement = dsAnn.Tables[0].Rows[0]["Announcement"].ToString();
                            }
                        }
                        else
                        {
                            UserSession.Announcement = string.Empty;
                        }
                        Response.Redirect("dashboard.aspx", true);
                    }
                }
            }
            catch (Exception ex)
            {
                //lblError.Text = ex.Message;
                errormsg(ex.Message);
            }
        }
        #endregion
    }
}