﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CarParkingSystem.Code;
using System.Globalization;
using PLCPS;
using BLCPS;
using System.Data;
using System.IO;

namespace CarParkingSystem
{
    public partial class Reports : PageBase
    {
        PLSlot _objPlslot = new PLSlot();
        BLSlot _objBlslot = new BLSlot();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string dFirstDayOfThisMonth = DateTime.Today.AddDays(-(DateTime.Today.Day - 1)).ToString("MM/dd/yyyy");
                DateTime firstOfNextMonth = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(1);
                string lastOfThisMonth = DateTime.Today.ToString("MM/dd/yyyy");
                this.txtfromdate.Text = dFirstDayOfThisMonth;
                this.txttodate.Text = lastOfThisMonth;
                Setheadertextdetails();
                btncheck_Click(null, null);
            }
        }

        /// <summary>
        /// Sets the Header and Sub Header based on the Link.
        /// </summary>
        private void Setheadertextdetails()
        {
            try
            {
                string mainmenu = string.Empty;
                string submenu = string.Empty;
                string[] file = Request.CurrentExecutionFilePath.Split('/');
                string fileName = file[file.Length - 1];
                Getmainandsubform(fileName, out mainmenu, out submenu);
                if (mainmenu.Trim() == string.Empty)
                    mainmenu = "Report";
                if (submenu.Trim() == string.Empty)
                    submenu = "Report";
                lmainheader.Text = mainmenu;
                lsubheader.Text = submenu;
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message.ToString(), Common.ErrorType.Error);
            }
        }

        protected void btncheck_Click(object sender, EventArgs e)
        {
            try
            {
                lblError.Text = string.Empty;
                divmsg.Visible = false;
                IFormatProvider culture = new CultureInfo("en-US", true);
                DateTime fromdate = DateTime.ParseExact(txtfromdate.Text, "MM/dd/yyyy", culture);
                DateTime todate = DateTime.ParseExact(txttodate.Text, "MM/dd/yyyy", culture);
                _objPlslot.locationid = UserSession.LocationIDCurrent;
                _objPlslot.FromDate = fromdate;
                _objPlslot.ToDate = todate;
                DataSet dsTransaction = new DataSet();
                dsTransaction = _objBlslot.GetReportDet(_objPlslot);
                if (dsTransaction != null && dsTransaction.Tables.Count > 0 && dsTransaction.Tables[0].Rows.Count > 0)
                {
                    rptrreport.DataSource = dsTransaction.Tables[0];
                    rptrreport.DataBind();
                }
                else
                {
                    rptrreport.DataSource = null;
                    rptrreport.DataBind();
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, Common.ErrorType.Error);
            }
        }

        private void Download(Repeater rptrdownload, string filename)
        {
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition",
             "attachment;filename=" + filename + ".xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            Table tb = new Table();


            TableCell cell3 = new TableCell();
            cell3.Controls.Add(rptrdownload);



            TableRow tr3 = new TableRow();
            tr3.Cells.Add(cell3);


            tb.Rows.Add(tr3);

            //}
            tb.RenderControl(hw);
            //style to format numbers to string
            string style = @"<style> .textmode { mso-number-format:\@; } </style>";
            Response.Write(style);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }

        public void NotifyMessages(string message, Common.ErrorType et)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (et == Common.ErrorType.Error)
            {
                divmsg.Attributes.Add("class", "alert alert-danger");
            }
            else if (et == Common.ErrorType.Information)
            {
                divmsg.Attributes.Add("class", "alert alert-success");
            }
        }

        protected void lnkdownload_Click(object sender, EventArgs e)
        {
            if (rptrreport.Items.Count > 0)
            {
                Download(rptrreport, "ParkingDetails");
            }
            else
            {
                NotifyMessages("No Data Found", Common.ErrorType.Error);
            }
        }
    }
}