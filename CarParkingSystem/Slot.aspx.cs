﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CarParkingSystem.Code;
using PLCPS;
using BLCPS;
using System.Data;
namespace CarParkingSystem
{
    public partial class Slot : PageBase
    {
        PLSlot _objPlslot = new PLSlot();
        BLSlot _objBlslot = new BLSlot();

        PLvariant _objPlvariant = new PLvariant();
        BLvariant _objBlvariant = new BLvariant();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ButtonStatus(Common.ButtonStatus.Cancel);
                BindSlot();
                Setheadertextdetails();
                Bindvarints();
            }
        }

        private void Bindvarints()
        {
            DataSet dsVariant = new DataSet();
            _objPlvariant.companyid = UserSession.CompanyIDCurrent;
            _objPlvariant.locationid = UserSession.LocationIDCurrent;
            _objPlvariant.variantid = 0;
            dsVariant = _objBlvariant.Getvariants(_objPlvariant);
            if (dsVariant != null && dsVariant.Tables.Count > 0 && dsVariant.Tables[0].Rows.Count > 0)
            {
                this.ddlvariant.DataSource = dsVariant.Tables[0];
                this.ddlvariant.DataTextField = "varianttype";
                this.ddlvariant.DataValueField = "ID";
                this.ddlvariant.DataBind();
            }
        }

        /// <summary>
        /// Sets the Header and Sub Header based on the Link.
        /// </summary>
        private void Setheadertextdetails()
        {
            try
            {
                string mainmenu = string.Empty;
                string submenu = string.Empty;
                string[] file = Request.CurrentExecutionFilePath.Split('/');
                string fileName = file[file.Length - 1];
                Getmainandsubform(fileName, out mainmenu, out submenu);
                if (mainmenu.Trim() == string.Empty)
                    mainmenu = "Master";
                if (submenu.Trim() == string.Empty)
                    submenu = "Slot";
                lmainheader.Text = mainmenu;
                lsubheader.Text = submenu;
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message.ToString(), Common.ErrorType.Error);
            }
        }

        protected void bNew_Click(object sender, EventArgs e)
        {
            ButtonStatus(Common.ButtonStatus.New);
        }

        protected void bSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (tslot.Text.Trim() == string.Empty)
                {
                    NotifyMessages("Enter Slot", Common.ErrorType.Error);
                    return;
                }

                if (tkeyno.Text.Trim() == string.Empty)
                {
                    NotifyMessages("Enter Key No", Common.ErrorType.Error);
                    return;
                }

                int variantid = 0;
                if (!Common.DDVal(ddlvariant, out variantid))
                {
                    NotifyMessages("Select Variant", Common.ErrorType.Error);
                    return;
                }
                int parkingtype = 0;
                Common.DDVal(ddltype, out parkingtype);

                _objPlslot.slotname = tslot.Text.Trim();
                _objPlslot.KeyId = tkeyno.Text.Trim();
                _objPlslot.VariantId = variantid;
                _objPlslot.ParkingType = parkingtype;
                _objPlslot.slotid = Convert.ToInt32(hdnslotid.Value);
                _objPlslot.companyid = UserSession.CompanyIDCurrent;
                _objPlslot.locationid = UserSession.LocationIDCurrent;
                _objPlslot.UserId = UserSession.UserID;
                string[] result = _objBlslot.InsertUpdateSlot(_objPlslot);
                if (result[1] != "-1")
                {
                    BindSlot();
                    ButtonStatus(Common.ButtonStatus.Cancel);
                    NotifyMessages(result[0], Common.ErrorType.Information);
                }
                else
                {
                    NotifyMessages(result[0], Common.ErrorType.Error);
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message.ToString(), Common.ErrorType.Error);
            }
        }

        private void BindSlot()
        {
            try
            {
                _objPlslot.slotid = 0;
                DataSet dsSlot = new DataSet();
                _objPlslot.companyid = UserSession.CompanyIDCurrent;
                _objPlslot.locationid = UserSession.LocationIDCurrent;
                _objPlslot.slotid = 0;
                _objPlslot.Type = "all";
                dsSlot = _objBlslot.GetSlotDetails(_objPlslot);
                if (dsSlot != null && dsSlot.Tables.Count > 0 && dsSlot.Tables[0].Rows.Count > 0)
                {
                    rptrslotname.DataSource = dsSlot.Tables[0];
                    rptrslotname.DataBind();
                }
                else
                {
                    rptrslotname.DataSource = null;
                    rptrslotname.DataBind();
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message.ToString(), Common.ErrorType.Error);
            }
        }

        protected void bCancel_Click(object sender, EventArgs e)
        {
            ButtonStatus(Common.ButtonStatus.Cancel);
        }

        protected void rptrslotname_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "edit")
                {
                    hdnslotid.Value = e.CommandArgument.ToString();
                    ButtonStatus(Common.ButtonStatus.Edit);
                    BindControls();
                }
                if (e.CommandName == "delete")
                {
                    int slotid = Convert.ToInt32(e.CommandArgument);
                    _objPlslot.slotid = slotid;
                    string result = _objBlslot.DeleteSlotDetails(_objPlslot);
                    NotifyMessages(result, Common.ErrorType.Information);
                    BindSlot();
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message.ToLower(), Common.ErrorType.Error);
            }
        }

        private void BindControls()
        {
            try
            {
                _objPlslot.slotid = Convert.ToInt32(hdnslotid.Value);
                _objPlslot.companyid = UserSession.CompanyIDCurrent;
                _objPlslot.locationid = UserSession.LocationIDCurrent;
                _objPlslot.Type = "all";
                DataSet dsSlotDet = new DataSet();
                dsSlotDet = _objBlslot.GetSlotDetails(_objPlslot);
                if (dsSlotDet != null && dsSlotDet.Tables.Count > 0 && dsSlotDet.Tables[0].Rows.Count > 0)
                {
                    DataRow drSlotdet = dsSlotDet.Tables[0].Rows[0];
                    tslot.Text = drSlotdet["slotname"].ToString();
                    tkeyno.Text = drSlotdet["keyno"].ToString();
                    if (drSlotdet["varianttype"].ToString().Trim() != string.Empty)
                    {
                        ddlvariant.SelectedValue = drSlotdet["varianttype"].ToString();
                    }
                    if (drSlotdet["parkingtype"].ToString().Trim() != string.Empty)
                    {
                        ddltype.SelectedValue = drSlotdet["parkingtype"].ToString();
                    }
                }
                else
                {
                    tslot.Text = string.Empty;
                    //tShortName.Text = string.Empty;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void NotifyMessages(string message, Common.ErrorType et)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (et == Common.ErrorType.Error)
            {
                divmsg.Attributes.Add("class", "alert alert-danger");
            }
            else if (et == Common.ErrorType.Information)
            {
                divmsg.Attributes.Add("class", "alert alert-success");
            }
        }

        private void ButtonStatus(Common.ButtonStatus status)
        {
            if (status == Common.ButtonStatus.New || status == Common.ButtonStatus.Cancel ||
                status == Common.ButtonStatus.Edit)
            {
                this.tslot.Text = tkeyno.Text = string.Empty;
            }
            this.bNew.Visible = false;
            this.divmsg.Visible = false;
            if (status == Common.ButtonStatus.New)
            {
                this.pAdd.Visible = true;
                this.bNew.Enabled = false;
                this.bSave.Enabled = true;
            }
            else if (status == Common.ButtonStatus.View)
            {
                this.pAdd.Visible = true;
                this.bNew.Enabled = false;
                this.bSave.Enabled = true;
                this.ddlvariant.Focus();
            }
            else if (status == Common.ButtonStatus.Edit)
            {
                this.ddlvariant.Focus();
                this.pAdd.Visible = true;

                this.bNew.Enabled = false;
                this.bSave.Enabled = true;
            }
            else if (status == Common.ButtonStatus.Cancel)
            {
                this.pAdd.Visible = false;

                this.bNew.Visible = true;
                this.bNew.Enabled = true;
                this.bSave.Enabled = false;
                hdnslotid.Value = "0";
            }
        }
    }
}