﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLCPS;
using PLCPS;
using CarParkingSystem.Code;
using System.Data;
using System.IO;

namespace CarParkingSystem
{
    public partial class dashboard : PageBase
    {
        PLSlot _objPlslot = new PLSlot();
        BLSlot _objBlslot = new BLSlot();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Setheadertextdetails();
                Binddashboard();
            }
        }

        public void NotifyMessages(string message, Common.ErrorType et)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (et == Common.ErrorType.Error)
            {
                divmsg.Attributes.Add("class", "alert alert-danger");
            }
            else if (et == Common.ErrorType.Information)
            {
                divmsg.Attributes.Add("class", "alert alert-success");
            }
        }

        /// <summary>
        /// Sets the Header and Sub Header based on the Link.
        /// </summary>
        private void Setheadertextdetails()
        {
            try
            {
                string mainmenu = string.Empty;
                string submenu = string.Empty;
                string[] file = Request.CurrentExecutionFilePath.Split('/');
                string fileName = file[file.Length - 1];
                Getmainandsubform(fileName, out mainmenu, out submenu);
                if (mainmenu.Trim() == string.Empty)
                    mainmenu = "Dashboard";
                if (submenu.Trim() == string.Empty)
                    submenu = "Dashboard";
                lmainheader.Text = mainmenu;
                lsubheader.Text = submenu;
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message.ToString(), Common.ErrorType.Error);
            }
        }


        private void Binddashboard()
        {
            try
            {
                _objPlslot.slotid = 0;
                DataSet dsdashboard = new DataSet();
                _objPlslot.companyid = UserSession.CompanyIDCurrent;
                _objPlslot.locationid = UserSession.LocationIDCurrent;
                _objPlslot.InOutDateTime = DateTime.Now;
                dsdashboard = _objBlslot.Getdashboarddetials(_objPlslot);
                if (dsdashboard != null && dsdashboard.Tables.Count > 0 && dsdashboard.Tables[0].Rows.Count > 0)
                {
                    ltotalslots.Text = dsdashboard.Tables[0].Rows[0][0].ToString();
                    ltotalalloted.Text = dsdashboard.Tables[0].Rows[0][1].ToString();
                    ltotalavailable.Text = dsdashboard.Tables[0].Rows[0][2].ToString();

                    if (dsdashboard.Tables[1].Rows.Count > 0)
                    {
                        rptralloted.DataSource = dsdashboard.Tables[1];
                        rptralloted.DataBind();
                    }
                    else
                    {
                        rptralloted.DataSource = null;
                        rptralloted.DataBind();
                    }

                    if (dsdashboard.Tables[2].Rows.Count > 0)
                    {
                        rptravilable.DataSource = dsdashboard.Tables[2];
                        rptravilable.DataBind();
                    }
                    else
                    {
                        rptravilable.DataSource = null;
                        rptravilable.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        protected void lnkavailable_Click(object sender, EventArgs e)
        {
            try
            {
                Download(rptravilable, "AvailableSlots");
            }
            catch (Exception ex)
            {
            }
        }

        protected void lnkalloted_Click(object sender, EventArgs e)
        {
            try
            {
                Download(rptralloted, "AllotedSlots");
            }
            catch (Exception ex)
            {
            }
        }

        private void Download(Repeater rptrdownload, string filename)
        {
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition",
             "attachment;filename=" + filename + ".xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            Table tb = new Table();


            TableCell cell3 = new TableCell();
            cell3.Controls.Add(rptrdownload);

           

            TableRow tr3 = new TableRow();
            tr3.Cells.Add(cell3);
      

            tb.Rows.Add(tr3);

            //}
            tb.RenderControl(hw);
            //style to format numbers to string
            string style = @"<style> .textmode { mso-number-format:\@; } </style>";
            Response.Write(style);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }


    }
}