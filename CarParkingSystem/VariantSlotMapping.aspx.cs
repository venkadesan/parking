﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CarParkingSystem.Code;
using PLCPS;
using BLCPS;
using System.Data;
using System.IO;
namespace CarParkingSystem
{
    public partial class VariantSlotMapping : PageBase
    {
        PLSlot _objPlslot = new PLSlot();
        BLSlot _objBlslot = new BLSlot();

        PLvariant _objPlvariant = new PLvariant();
        BLvariant _objBlvariant = new BLvariant();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Setheadertextdetails();
                Bindvarints();
                BindMappedDetails();
            }
        }
        /// <summary>
        /// Sets the Header and Sub Header based on the Link.
        /// </summary>
        private void Setheadertextdetails()
        {
            try
            {
                string mainmenu = string.Empty;
                string submenu = string.Empty;
                string[] file = Request.CurrentExecutionFilePath.Split('/');
                string fileName = file[file.Length - 1];
                Getmainandsubform(fileName, out mainmenu, out submenu);
                if (mainmenu.Trim() == string.Empty)
                    mainmenu = "Master";
                if (submenu.Trim() == string.Empty)
                    submenu = "Mapping";
                lmainheader.Text = mainmenu;
                lsubheader.Text = submenu;
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message.ToString(), Common.ErrorType.Error);
            }
        }

        private void Bindvarints()
        {
            DataSet dsVariant = new DataSet();
            _objPlvariant.companyid = UserSession.CompanyIDCurrent;
            _objPlvariant.locationid = UserSession.LocationIDCurrent;
            _objPlvariant.variantid = 0;
            dsVariant = _objBlvariant.Getvariants(_objPlvariant);
            if (dsVariant != null && dsVariant.Tables.Count > 0 && dsVariant.Tables[0].Rows.Count > 0)
            {
                this.ddlvariant.DataSource = dsVariant.Tables[0];
                this.ddlvariant.DataTextField = "varianttype";
                this.ddlvariant.DataValueField = "ID";
                this.ddlvariant.DataBind();
            }
        }

        protected void ddltype_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindMappedDetails();
        }

        protected void ddlvariant_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindMappedDetails();
        }

        public void NotifyMessages(string message, Common.ErrorType et)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (et == Common.ErrorType.Error)
            {
                divmsg.Attributes.Add("class", "alert alert-danger");
            }
            else if (et == Common.ErrorType.Information)
            {
                divmsg.Attributes.Add("class", "alert alert-success");
            }
        }

        private void BindMappedDetails()
        {
            try
            {
                lblError.Text = string.Empty;
                divmsg.Visible = false;
                int variantid = 0;
                Common.DDVal(ddlvariant, out variantid);
                int typeid = 0;
                Common.DDVal(ddltype, out typeid);
                _objPlslot.VariantId = variantid;
                _objPlslot.ParkingType = typeid;
                DataSet dsfreeMapped = new DataSet();
                _objPlslot.companyid = UserSession.CompanyIDCurrent;
                _objPlslot.locationid = UserSession.LocationIDCurrent;
                dsfreeMapped = _objBlslot.GetFreeAndMappedSlot(_objPlslot);
                if (dsfreeMapped != null && dsfreeMapped.Tables.Count > 0 && dsfreeMapped.Tables[1].Rows.Count > 0)
                {
                    this.lstslots.DataSource = dsfreeMapped.Tables[1];
                    this.lstslots.DataTextField = "slotname";
                    this.lstslots.DataValueField = "slotid";
                    this.lstslots.DataBind();
                }
                else
                {
                    lstslots.Items.Clear();
                }
                if (dsfreeMapped != null && dsfreeMapped.Tables.Count > 0 && dsfreeMapped.Tables[0].Rows.Count > 0)
                {
                    rptrslot.DataSource = dsfreeMapped.Tables[0];
                    rptrslot.DataBind();
                }
                else
                {
                    rptrslot.DataSource = null;
                    rptrslot.DataBind();
                }

            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, Common.ErrorType.Error);
            }
        }

        protected void bSave_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dtSelectedValue = new DataTable("Slot");
                dtSelectedValue.Columns.Add("SlotId", typeof(int));
                foreach (System.Web.UI.WebControls.ListItem li in lstslots.Items)
                {
                    if (li.Selected == true)
                    {
                        dtSelectedValue.Rows.Add(Convert.ToInt32(li.Value));
                    }
                }
                foreach (RepeaterItem riLocDet in rptrslot.Items)
                {
                    HiddenField hdnslotid = (HiddenField)riLocDet.FindControl("hdnslotid");
                    CheckBox chcksave = (CheckBox)riLocDet.FindControl("chkslot");
                    if (hdnslotid != null && chcksave != null)
                    {
                        if (chcksave.Checked)
                        {
                            dtSelectedValue.Rows.Add(Convert.ToInt32(hdnslotid.Value));
                        }
                    }
                }

                string xmlString = string.Empty;
                using (TextWriter writer = new StringWriter())
                {
                    dtSelectedValue.TableName = "Slot";
                    dtSelectedValue.WriteXml(writer);
                    xmlString = writer.ToString();
                }
                _objPlslot.companyid = UserSession.CompanyIDCurrent;
                _objPlslot.locationid = UserSession.LocationIDCurrent;
                _objPlslot.SlotXML = xmlString;

                int variantid = 0, parkingid = 0;
                Common.DDVal(ddlvariant, out variantid);
                Common.DDVal(ddltype, out parkingid);

                _objPlslot.VariantId = variantid;
                _objPlslot.ParkingType = parkingid;
                string[] result = _objBlslot.MapMultipleSlots(_objPlslot);
                if (result[1] != "-1")
                {
                    BindMappedDetails();
                    NotifyMessages(result[0], Common.ErrorType.Information);
                }
                else
                {
                    NotifyMessages(result[0], Common.ErrorType.Error);
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, Common.ErrorType.Error);
            }
        }
    }
}