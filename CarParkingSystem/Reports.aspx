﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true"
    CodeBehind="Reports.aspx.cs" Inherits="CarParkingSystem.Reports" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content" runat="server">
    <ul id="nav-info" class="clearfix">
        <li>
            <asp:Literal ID="lmainheader" runat="server" Text="Master"></asp:Literal></li>
        <li>
            <asp:Literal ID="lsubheader" runat="server" Text="Slot"></asp:Literal></li>
    </ul>
    <!-- /submenu -->
    <!-- content main container -->
    <div class="alert alert-danger" id="divmsg" visible="false" runat="server">
        <button type="button" class="close" data-dismiss="alert">
            ×</button>
        <asp:Label runat="server" ID="lblError"></asp:Label>
    </div>
    <div class="form-horizontal form-box">
        <h4 class="form-box-header">
            Report</h4>
        <div class="form-box-content">
            <asp:Panel runat="server" ID="pAdd">
                <div class="form-group">
                    <label class="control-label col-md-2">
                        <asp:Label Text="From Date" runat="server" ID="Label4" />:</label>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtfromdate" CssClass="form-control input-datepicker" runat="server"></asp:TextBox>
                    </div>
                    <label class="control-label col-md-2">
                        <asp:Label Text="To Date" runat="server" ID="Label3" />:</label>
                    <div class="col-md-3">
                        <asp:TextBox ID="txttodate" CssClass="form-control input-datepicker" runat="server"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:Button ID="btncheck" runat="server" CssClass="btn btn-success" Text="Search"
                            OnClick="btncheck_Click" />
                    </div>
                </div>
            </asp:Panel>
            <div class="form-group">
                <div class="col-md-1" style="float: right">
                    <asp:LinkButton ID="lnkdownload" runat="server" ToolTip="To Download XL Document"
                        OnClick="lnkdownload_Click">
                    <i class="gemicon-medium-download"></i></asp:LinkButton>
                </div>
            </div>
            <div class="row">
                <asp:Repeater ID="rptrreport" runat="server">
                    <HeaderTemplate>
                        <table id="example-datatables" class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr class="submenu new-color">
                                    <th>
                                        S.No
                                    </th>
                                    <th>
                                        In-Date
                                    </th>
                                    <th>
                                        In-Time
                                    </th>
                                    <th>
                                        In - Driver
                                    </th>
                                    <th>
                                        Out-Date
                                    </th>
                                    <th>
                                        Out-Time
                                    </th>
                                    <th>
                                        Out-Driver
                                    </th>
                                    <th>
                                        RFID CardNo
                                    </th>
                                    <th>
                                        Slot
                                    </th>
                                    <th>
                                        Reg.No
                                    </th>
                                    <th>
                                        Vehicle Variant
                                    </th>
                                    <th>
                                        Parking Type
                                    </th>
                                </tr>
                            </thead>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <asp:Label ID="Label2" runat="server" Text='<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>' />
                            </td>
                            <td>
                                <asp:Label ID="InDate" runat="server" Text='<%# Eval("InDate") %>' ToolTip='<%# Eval("InDate") %>' />
                            </td>
                            <td>
                                <asp:Label ID="InTime" runat="server" Text='<%# Eval("InTime") %>' ToolTip='<%# Eval("InTime") %>' />
                            </td>
                            <td>
                                <asp:Label ID="InDrivername" runat="server" Text='<%# Eval("InDrivername") %>' ToolTip='<%# Eval("InDrivername") %>' />
                            </td>
                            <td>
                                <asp:Label ID="OutDate" runat="server" Text='<%# Eval("OutDate") %>' ToolTip='<%# Eval("OutDate") %>' />
                            </td>
                            <td>
                                <asp:Label ID="OutTime" runat="server" Text='<%# Eval("OutTime") %>' ToolTip='<%# Eval("OutTime") %>' />
                            </td>
                            <td>
                                <asp:Label ID="OutDrivername" runat="server" Text='<%# Eval("OutDrivername") %>'
                                    ToolTip='<%# Eval("OutDrivername") %>' />
                            </td>
                            <td>
                                <asp:Label ID="RFICardNo" runat="server" Text='<%# Eval("RFICardNo") %>' ToolTip='<%# Eval("RFICardNo") %>' />
                            </td>
                            <td>
                                <asp:Label ID="slotname" runat="server" Text='<%# Eval("slotname") %>' ToolTip='<%# Eval("slotname") %>' />
                            </td>
                            <td>
                                <asp:Label ID="RegNo" runat="server" Text='<%# Eval("RegNo") %>' ToolTip='<%# Eval("RegNo") %>' />
                            </td>
                            <td>
                                <asp:Label ID="varianttype" runat="server" Text='<%# Eval("varianttype") %>' ToolTip='<%# Eval("varianttype") %>' />
                            </td>
                            <td>
                                <asp:Label ID="ParkingType" runat="server" Text='<%# Eval("ParkingType") %>' ToolTip='<%# Eval("ParkingType") %>' />
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>
</asp:Content>
